package com.jokeur.autosimulator3000;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.jokeur.autosimulator3000.aide.Tools;

/**
 * Unit test for simple App.
 */
public class ToolsTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ToolsTest(String testName)
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( ToolsTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testGetProperties() {
        String propertiesTest = Tools.getProperty("test");
        assertEquals( "success", propertiesTest);
        String ressourceTest = Tools.getProperty("test", "ressources.properties");
        assertEquals( "success", ressourceTest);
    }
}
