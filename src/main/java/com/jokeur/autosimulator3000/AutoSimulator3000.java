package com.jokeur.autosimulator3000;

import com.jokeur.autosimulator3000.controlleur.ConnexionScene;
import com.jokeur.autosimulator3000.controlleur.MainScene;
import com.jokeur.autosimulator3000.controlleur.ManipScene;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.geometry.Rectangle2D;
import javafx.stage.StageStyle;
import java.sql.Connection;
import javafx.stage.WindowEvent;
import javafx.event.EventHandler;
import javafx.animation.AnimationTimer;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import java.io.FileInputStream;
import com.jokeur.autosimulator3000.controlleur.Musique;
import java.io.*;

//classes du projet
import com.jokeur.autosimulator3000.modele.dao.DAOConnexion;

public class AutoSimulator3000 extends Application {


    public static void main(String[] args) {
        System.out.println( "Main method inside Thread : " +  Thread.currentThread().getName());
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        // test DB Connection
        DAOConnexion.testConnection();

        // Taille de la fenêtre
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

        // Options de la fenêtre
        stage.setX(primaryScreenBounds.getMinX());
        stage.setY(primaryScreenBounds.getMinY());
        stage.setWidth(primaryScreenBounds.getWidth());
        stage.setHeight(primaryScreenBounds.getHeight());
        stage.setTitle("Auto Simulator 3000");
        stage.initStyle(StageStyle.UNDECORATED);

        /***************************************************/

        ManipScene.setConnexionScene(stage);
        //stage.setScene(ManipScene.setInscriptionScene(stage));
        //stage.setScene(ManipScene.setMainScene(stage));
        stage.show();

        // JOUER LA MUSIQUE ALEATOIREMENT
        Musique.playRandom();

        new AnimationTimer() {

            @Override

            public void handle(long now) {
                switch(ManipScene.getSceneActuelle()) {
                    case 1:
                        //gestion de l'animation dans la main scene
                        MainScene.mainAnimation(stage);
                        break;
                    case 2:
                        //gestion de l'animation dans la scene inscription
                        break;
                    default:
                        //gestion de l'animation dans la scene connexion : par defaut
                        ConnexionScene.parcourDemo();

                }
            }

        }.start();
    }
}