package com.jokeur.autosimulator3000.aide;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import com.jokeur.autosimulator3000.controlleur.MainScene;
import com.jokeur.autosimulator3000.controlleur.ManipScene;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.util.Properties;
import java.io.FileInputStream;
import javafx.scene.Group;
import java.util.List;
import java.util.ArrayList;
import com.jokeur.autosimulator3000.controlleur.ConnexionScene;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import com.jokeur.autosimulator3000.modele.Stop;

public class Tools {
    /**
     * retourne une liste des couleur disponible dans javafx
     * @return ObservableList<String>
     */
    public static double largeurRoute;
    public static Stop stop;

    public static ObservableList<String> getJavaFXColors() {
        Field[] declaredFields = Color.class.getDeclaredFields();
        Map<String, Color> colorsMap = new HashMap<>();
        for (Field field : declaredFields) {
            if (Modifier.isStatic(field.getModifiers()) && Modifier.isPublic(field.getModifiers())) {
                try {
                    colorsMap.put(field.getName(), (Color)field.get(null));
                } catch (Exception ex) {
                    System.out.println("fail : " + ex.getMessage());
                }
            }
        }

        ObservableList<String> colors = FXCollections.observableArrayList(colorsMap.keySet());
        FXCollections.sort(colors);

        return colors;
    }

    //pour ne pas avoir a modifier les fonctions existantes on surcharge la fonction
    public static String getProperty(String cle) {
        return getProperty(cle, "config.properties");
    }

    public static String getProperty(String cle, String config) {
        try {
            Properties properties = new Properties();
            //Ouverture du fichier config / Récupération de la config
            InputStream fileStream = Tools.class.getResourceAsStream("/" + config);
            properties.load(fileStream);
            fileStream.close();

            return properties.getProperty(cle);
        }
        catch (Exception e) {
            System.out.println("Erreur lors de la recuperation des proprietees : " + e.getMessage());
            return null;
        }
    }

    public static Group genererCarteDefaut(double largeurConteneur, double hauteurConteneur, boolean routeVisible) {
        Image imgRouteX = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgRouteX", "ressources.properties")).toExternalForm());
        Image imgRouteY = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgRouteY", "ressources.properties")).toExternalForm());
        Image imgInter = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgInter", "ressources.properties")).toExternalForm());
        Image imgVoiture = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgVoiture", "ressources.properties")).toExternalForm());

        List<Double[]> listRoutesVert = new ArrayList();
        List<Double[]> listRoutesHor = new ArrayList();
        largeurRoute = hauteurConteneur*0.05;

        Group groupeCarte = new Group();
        //on créé les coordonnées des routes de la carte "par defaut"
        // VERTICALES (Y)

        //on défini les données de départ
        double xMin = largeurConteneur * 0.15;
        double yMin = hauteurConteneur * 0.05;
        double yMax = hauteurConteneur * 0.95;

        for(int i = 0; i < 5; i++) {
            //on met dans une liste les coordonnées de la route verticale (xMin, yMin, xMax, yMax)
            listRoutesVert.add(new Double[]{xMin, yMin, xMin + largeurRoute, yMax});
            //on décalle la route d'une valeur arbitraire (calculé pour que les routes soit environ espacée de la même valeur)
            xMin = xMin + largeurConteneur * 0.15;
        }

        // HORIZONTALES (X)
        yMin = hauteurConteneur * 0.15;
        xMin = largeurConteneur * 0.05;
        double xMax = largeurConteneur * 0.95;
        for(int i = 0; i < 5; i++) {
            //on met dans une liste les coordonnées de la route horizontale (xMin, yMin, xMax, yMax)
            listRoutesHor.add(new Double[]{xMin, yMin, xMax, yMin + largeurRoute});
            //on décalle la route d'une valeur arbitraire (calculé pour que les routes soit environ espacée de la même valeur)
            yMin = yMin + hauteurConteneur * 0.15;
        }

        /***************************************************/
        // ROUTES MISE EN PLACE
        Image img;
        //on defini l'image de la route verticale : si elle est null la route n'est pas visible
        img = routeVisible ? imgRouteY : null;
        //on créé les routes
        for(Double[] coords : listRoutesHor) {
            groupeCarte.getChildren().add(makeRoute(coords, img));
        }
        //on defini l'image de la route horizontale : si elle est null la route n'est pas visible
        img = routeVisible ? imgRouteX : null;
        for(Double[] coords : listRoutesVert) {
            groupeCarte.getChildren().add(makeRoute(coords, img));
        }
        double x, y;
        img = routeVisible ? imgInter : null;
        //on créé les inteersection
        for(Double[] horCoords : listRoutesHor) {
            //on récupère le yMin de la route horizontale
            y = horCoords[1];
            for(Double[] vertCoords : listRoutesVert) {
                //on récupère le xMin de la route verticale
                x = vertCoords[0];
                //on crée un carré de coté largeurRoute et position a x,y
                groupeCarte.getChildren().add(makeIntersection(x, y, largeurRoute, img));
                String imgStopX = "imgStopX";
                String imgStopY = "imgStopY";
                //if(routeVisible && MainScene.listeEvenement.isEmpty()) {
                if(routeVisible && MainScene.listeEvenement.size() < 5) {
                    if(Math.random() > 0.8) {
                        double rand = Math.random();
                        if(rand < 0.33) {
                            groupeCarte.getChildren().add(MainScene.creerEvenementCarteAuto(x, y, largeurRoute, "stopY"));
                        } else if (rand >= 0.33 && rand < 0.66){
                            groupeCarte.getChildren().add(MainScene.creerEvenementCarteAuto(x, y, largeurRoute, "stopX"));
                        } else {
                            groupeCarte.getChildren().add(MainScene.creerEvenementCarteAuto(x, y, largeurRoute, "feu"));
                        }
                    }

                }
            }
        }

        return groupeCarte;
    }


    public static Rectangle makeRoute(Double[] coords, Image img) {
        double xMin = coords[0];
        double yMin = coords[1];
        double xMax = coords[2];
        double yMax = coords[3];

        Rectangle route = new Rectangle(xMin, yMin, (xMax - xMin), (yMax - yMin));
        if(null == img) {
            route.setFill(Color.TRANSPARENT);
        } else {
            route.setFill(new ImagePattern(img));
        }
        return route;
    }

    public static Rectangle makeIntersection(double x, double y, double largeurRoute, Image img) {
        Rectangle intersection = new Rectangle(x, y, largeurRoute, largeurRoute);
        if(null == img) {
            intersection.setFill(Color.TRANSPARENT);
        } else {
            intersection.setFill(new ImagePattern(img));
        }
        intersection.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t) {
                //on vérifie qu'on est sur la scene principale
                if(ManipScene.getSceneActuelle() == 1) {
                    //en fonction de l'action choisie on agit differemment
                    MainScene.creerEvenement(intersection.getBoundsInLocal().getMinX(), intersection.getBoundsInLocal().getMinY());
                }
            }
        });
        return intersection;
    }

    public static Rectangle makeVehicule(double x, double y, double largeurRoute, Image img) {
        Rectangle vehicule = new Rectangle(x, y, largeurRoute, largeurRoute);//x, y, largeurRoute, largeurRoute*0.5
        vehicule.setFill(new ImagePattern(img));
        return vehicule;
    }

    public static Rectangle makeVehicule2(double x, double y, double largeurRoute, Image img) {
        Rectangle vehicule2 = new Rectangle(x, y, largeurRoute*0.5, largeurRoute);
        vehicule2.setFill(new ImagePattern(img));
        return vehicule2;   
    }

    // Fonction qui renvoie vrai si la chaine donnée en paramètre est vide
    public static boolean isBlank(String str){
        return null == str || "".equals(str);
    }

    //fonction pour arrondir a deux decimal au suppérieur
    public static double arrondi(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
