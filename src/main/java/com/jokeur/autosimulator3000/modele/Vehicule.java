package com.jokeur.autosimulator3000.modele;

import com.jokeur.autosimulator3000.aide.Tools;
import com.jokeur.autosimulator3000.controlleur.ConnexionScene;
import com.jokeur.autosimulator3000.controlleur.MainScene;
import com.jokeur.autosimulator3000.controlleur.ManipScene;
import com.jokeur.autosimulator3000.controlleur.Bruitage;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Region;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.MouseButton;

import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

import java.util.Date;

public class Vehicule extends Rectangle implements interact {
    //Variables de classe
    private static int indexVehicule = 0;

    //caractéristiques physiques
    private int poids;
    //en px
    private double longueur;
    private int imageIndex;
    private String couleur;

    //caractéristiques techniques
    //px/ms
    private double vitesseMax;
    //px/ms/ms
    private double accelerationMax;
    //correspond au pourcentage d'acceleration pour simuler la conduite pour calculer l'acceleration
    private double conduite = 0.1;
    //px/ms/ms
    private double freinageMax;
    //correspond au pourcentage de freinage pour calculer le freinage
    //force par defaut 100%
    private double freinageForce = 1;
    //variable d'arrêt
    //variable permettant de savoir depuis combien de temps on marque l'arret a un stop
    private Long marquage = null;

    //variables techniques
    //en px/ms
    private double vitesse = 0;
    private boolean axeX = false;
    private boolean axeY = false;
    private int directionSens;

    //variable de collision
    private Rectangle2D hitbox;
    private Rectangle2D radar;
    private double rayonRadar = 10;
    private String type = "vehicule";

    //variable statiques
    public static Image imgCarUp = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgCarUp", "ressources.properties")).toExternalForm());
    public static ImagePattern imgPatternCarUp = new ImagePattern(imgCarUp);
    public static Image imgCarDown = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgCarDown", "ressources.properties")).toExternalForm());
    public static ImagePattern imgPatternCarDown = new ImagePattern(imgCarDown);
    public static Image imgCarLeft = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgCarLeft", "ressources.properties")).toExternalForm());
    public static ImagePattern imgPatternCarLeft = new ImagePattern(imgCarLeft);
    public static Image imgCarRight = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgCarRight", "ressources.properties")).toExternalForm());
    public static ImagePattern imgPatternCarRight = new ImagePattern(imgCarRight);

    //Evaluation parcours
    private Boolean arretAuStopEffectue = null;
    private boolean arretAuFeu = false;
    private boolean arretAuStopDemande = false;
    private boolean arretAuStopNonDemande = false;
    private boolean arretAuFeuDemande = false;
    private boolean arretAuFeuNonDemande = false;
    private Evenement interactFirst;
    private Evenement controleEnCours = null;
    private int evaluation = 0;
    private int evaluationMax = 500;
    private boolean controleEffectue = false;

    public Vehicule(int p_poids, String p_couleur, double p_largeurRoute) {
        Vehicule that = this;

        setId("vehicule" + indexVehicule);
        indexVehicule++;

        that.poids = p_poids;
        setDirection(0);
        that.couleur = p_couleur;

        that.setHeight(p_largeurRoute);
        that.setWidth(p_largeurRoute);

        longueur = 2.5;
        
        vitesseMax = convertirKmHEnPxMs(50);
        accelerationMax = convertirKmHEnPxMs(20);
        freinageMax = convertirKmHEnPxMs(50);
        marquage = null;

        //reset des variables
        resetVars();
        //collision
        makeCollisionsBox();

        this.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if (!MainScene.animationStarted){
                  if (t.getButton() == MouseButton.SECONDARY) {
                      MainScene.supprimerEvenementParId(that.getId());
                  }
                  else {
                      that.setDirection((that.imageIndex+1)%4);
                  }
                }
            }
        });
    }

    public String getType() {
        return this.type;
    }

    /**
     * Créé un Rectangle2D incluant les méthodes de collisons
     */
    private void makeCollisionsBox() {
        hitbox = new Rectangle2D(this.getX(), this.getY(), this.getWidth(), this.getHeight());

        double xMinRadar = this.getX() - convertirMetreEnPx(this.rayonRadar);
        double yMinRadar = this.getY() - convertirMetreEnPx(this.rayonRadar);
        double largeurRadar = this.getWidth() + convertirMetreEnPx(this.rayonRadar*2);
        double hauteurRadar = this.getHeight() + convertirMetreEnPx(this.rayonRadar*2);
        radar = new Rectangle2D(xMinRadar, yMinRadar, largeurRadar, hauteurRadar);

        /*Rectangle radarVisible = (Rectangle) ManipScene.getScene().lookup("#radarVisible");
        radarVisible.setWidth(largeurRadar);
        radarVisible.setHeight(hauteurRadar);
        radarVisible.setY(yMinRadar);
        radarVisible.setX(xMinRadar);
        radarVisible.setVisible(true);*/
    }

    public void setVitesseMax(int vitesseSelec) {
        this.vitesseMax = convertirKmHEnPxMs(vitesseSelec);
    }

    public double getVitesseMax() {
        return this.vitesseMax;
    }

    /**
     * défini la position X du véhicule et recréé la hitbox
     * @param newX
     */
    public void setVehiculeX(double newX) {
        this.setX(newX);
        makeCollisionsBox();
    }

    /**
     * défini la position Y du véhicule et recréé la hitbox
     * @param newY
     */
    public void setVehiculeY(double newY) {
        this.setY(newY);
        makeCollisionsBox();
    }

    /**
     * donne le sens de circulation de la voiture
     * @return boolean : true : Circule dans le sens de l'axe X
     */
    public boolean isAxeX(){

        return this.axeX;
    }

    /**
     * défini le sens de circulation de la voiture à X
     * @param valeur boolean
     */
    public void setAxeX(boolean valeur){
        this.axeX = valeur;
        this.axeY = !this.axeX;
    }

    /**
     * donne le sens de circulation de la voiture
     * @return boolean : true : Circule dans le sens de l'axe Y
     */
    public boolean isAxeY(){
        return this.axeY;
    }

    /**
     * défini le sens de circulation de la voiture à Y
     * @param valeur boolean
     */
    public void setAxeY(boolean valeur){
        this.axeY = valeur;
        this.axeX = !this.axeY;
    }

    public int getDirectionSens(){
        return this.directionSens;
    }

    public void setDirectionSens(int valeur){
        this.directionSens = valeur;
    }

    public Rectangle2D getHitbox() {
        return this.hitbox;
    }

    /**
     * on ajoute la valeur de l'acceleration à la vitesse afin de simuler une accélération
     * si la vitesse dépasse la vitesse max on la corrige à vitesse max
     */
    public void accelerer() {
        this.vitesse += this.accelerationMax*conduite;
        if(this.vitesse > this.vitesseMax) {
            this.vitesse = this.vitesseMax;
        }
        if(this.vitesse == this.vitesseMax){
            Bruitage.sonRoule();
        }
    }

    /**
     * on retire la valeur du freinage à la vitesse afin de simuler une décélération
     * si la vitesse deviens négative on la corrige à 0
     */
    public void freiner() {
        this.vitesse -= this.freinageMax*this.freinageForce;
        if(this.vitesse < 0) {
            this.vitesse = 0;
        }

    }

    /**
     * on commence par determiner quel evenement sera celui à traiter en premier
     * ensuite en fonction du type d'evenement on à une reaction différente du véhicule
     */
    public void controles(){
        String msg;
        //si le vehicule sort de la carte on arrete le test
        if(MainScene.carteBox.contains(this.hitbox)) {
            for(Evenement evenement : MainScene.listeEvenement) {
                //on ne controle que les evenements detectable par le radar
                if(this.radar.intersects(evenement.getHitbox())) {
                    if(laCibleEstSurLeMemeAxe(evenement)
                        && null != calculDistance(evenement) && calculDistance(evenement) > 0
                        && ((null != interactFirst && evenement != interactFirst && calculDistance(evenement) < calculDistance(interactFirst)) || null == interactFirst)) {
                            interactFirst = evenement;
                    }
                }
            }
            if(null != interactFirst) {
                if(null == controleEnCours) {
                    controleEnCours = interactFirst;
                }
                switch(interactFirst.getType()) {
                    case "vehicule":
                        this.marquage = null;
                        interactFirst = null;
                        break;
                    case "up":
                        this.marquage = null;
                        if(null != calculDistance(interactFirst) && calculDistance(interactFirst) <= this.vitesse) {
                            this.setDirection(0);
                            interactFirst = null;
                        }
                        this.accelerer();
                        break;
                    case "right":
                        this.marquage = null;
                        if(null != calculDistance(interactFirst) && calculDistance(interactFirst) <= this.vitesse) {
                            this.setDirection(1);
                            interactFirst = null;
                        }
                        this.accelerer();
                        break;
                    case "down":
                        this.marquage = null;
                        if(null != calculDistance(interactFirst) && calculDistance(interactFirst) <= this.vitesse) {
                            this.setDirection(2);
                            interactFirst = null;
                        }
                        this.accelerer();
                        break;
                    case "left":
                        this.marquage = null;
                        if(null != calculDistance(interactFirst) && calculDistance(interactFirst) <= this.vitesse) {
                            this.setDirection(3);
                            interactFirst = null;
                        }
                        this.accelerer();
                        break;
                    case "flag":
                        this.marquage = null;
                        if(null != calculDistance(interactFirst) && calculDistance(interactFirst) > this.vitesse && calculDistance(interactFirst) <= calculDistanceFreinage()) {
                            this.freiner();
                            if(vitesse == 0) {
                                Bruitage.sonRouleStop();
                                Bruitage.sonWin();
                                evaluation += 500;
                                MainScene.switchAnimation();
                                msg = "Evaluation terminée avec succès. Evaluation : " + this.evaluation / this.evaluationMax*100 + "% (" + this.evaluation + " points sur "+ this.evaluationMax +").";
                                Alert alert = new Alert(Alert.AlertType.NONE, msg, ButtonType.FINISH);
                                alert.getDialogPane().getChildren().stream().filter(node -> node instanceof Label).forEach(node -> ((Label)node).setMinHeight(Region.USE_PREF_SIZE));
                                alert.show();
                                resetVars();
                            }
                        } else {
                            this.accelerer();
                        }
                        break;
                    case "stop":
                        if(isAxeX() && "X".equals(interactFirst.getStatut())) {
                            gestionEvtStop(interactFirst);
                        } else {
                            this.accelerer();
                        }
                        if(isAxeY() && "Y".equals(interactFirst.getStatut())) {
                            gestionEvtStop(interactFirst);
                        } else {
                            this.accelerer();
                        }
                        if(interactFirst.getHitbox().contains(this.hitbox)) {
                            interactFirst = null;
                            arretAuStopEffectue = null;
                        }
                        break;
                    case "feu":
                        String statutFeu = interactFirst.getStatut();
                        char couleurFeuConcerne;
                        if (isAxeX()){
                            couleurFeuConcerne = statutFeu.charAt(0);
                            gestionEvtFeu(couleurFeuConcerne);
                        } else {
                            couleurFeuConcerne = statutFeu.charAt(1);
                            gestionEvtFeu(couleurFeuConcerne);
                        }
                        if(interactFirst.getHitbox().contains(this.hitbox)) {
                            interactFirst = null;
                            arretAuFeu = false;
                        }
                        break;
                    default:
                        this.accelerer();
                }
            } else {
                this.accelerer();
            }
            this.avancer();

        } else {
            //on arrete le jeu
            Bruitage.sonRouleStop();
            Bruitage.sonAccident();
            evaluation -= 500;
            MainScene.switchAnimation();
            msg = "Echec évaluation. Evaluation : " + this.evaluation / this.evaluationMax*100 + "% (" + this.evaluation + " points sur "+ this.evaluationMax +").";
            Alert alert = new Alert(Alert.AlertType.NONE, msg, ButtonType.FINISH);
            alert.getDialogPane().getChildren().stream().filter(node -> node instanceof Label).forEach(node -> ((Label)node).setMinHeight(Region.USE_PREF_SIZE));
            alert.show();
            resetVars();
        }
        this.evaluationParcours();
    }

    /**
     * en fonction de l'axe de circulation et du sens de circulation on déplace le vehicule
     * si axe X et direction négative : on déplace le x du véhicule de la valeur de la vitese vers 0
     * si axe X et direction positive : on déplace le x du véhicule de la valeur de la vitese vers l'infini
     * si axe Y et direction négative : on déplace le y du véhicule de la valeur de la vitese vers 0
     * si axe Y et direction positive : on déplace le y du véhicule de la valeur de la vitese vers l'infini
     */
    public void avancer() {
        //int sens = getDirectionSens();
        if (this.isAxeY()){
            //double yActuel = this.getY();
            //double newPositionY = (yActuel + 0.5) * sens;
            //this.setY(newPositionY);
            this.setVehiculeY(this.getY() + (this.vitesse*this.getDirectionSens()));
        }
        else {
            //double xActuel = this.getX();
            //double newPositionX = (xActuel + 0.5) * sens;
            //setX(newPositionX);
            this.setVehiculeX(this.getX() + (this.vitesse*this.getDirectionSens()));
        }
    }

    /**
     * permet d'évaluer le parcours, de lui une note et une note maximal
     * on commence par déterminer quel type de controle est demandé
     * ensuite en fonction du type de controle on vérifie que le véhicule se comporte comme prévue
     */
    public void evaluationParcours() {
        try {
            //si un controle est en cours et que le controle n'a pas ete effectué : on controle
            if(null != controleEnCours && !controleEffectue) {

                switch(controleEnCours.getType()) {
                    //si le l'evenement courant n'est pas a controller on remet controleEnCours à null
                    case "vehicule":
                    case "up":
                    case "right":
                    case "down":
                    case "left":
                    case "flag":
                        controleEnCours = null;
                        break;
                    //pour les stop selon le sens on controle l'arret ou l'absence d'arret
                    case "stop":
                        if(isAxeX() && "X".equals(controleEnCours.getStatut())) {
                            if(!arretAuStopDemande) {
                                arretAuStopDemande = true;
                            }
                        } else {
                            if(!arretAuStopNonDemande) {
                                arretAuStopNonDemande = true;
                            }
                        }
                        if(isAxeY() && "Y".equals(controleEnCours.getStatut())) {
                            if(!arretAuStopDemande) {
                                arretAuStopDemande = true;
                            }
                        } else {
                            if(!arretAuStopNonDemande) {
                                arretAuStopNonDemande = true;
                            }
                        }
                        break;
                    case "feu":
                        char feuConcerne;
                        if(this.isAxeX()) {
                            feuConcerne = controleEnCours.getStatut().charAt(0);
                        } else {
                            feuConcerne = controleEnCours.getStatut().charAt(1);
                        }

                        switch (feuConcerne) {
                            case 'V':
                                arretAuFeuNonDemande = true;
                                arretAuFeuDemande = false;
                                break;
                            case 'O':
                            case 'R':
                                arretAuFeuNonDemande = false;
                                arretAuFeuDemande = true;
                                break;
                        }
                        break;
                    default:
                        break;
                }
                //pour controle l'arret au stop on verifie que la vitesse == 0 avant de passer sur le stop
                if(arretAuStopDemande || arretAuFeuDemande) {
                    if (this.vitesse > 0 && hitbox.intersects(controleEnCours.getHitbox())) {
                        evaluationMax += 100;
                        evaluation -= 100;
                        controleEffectue = true;

                        if(arretAuFeuDemande) {
                            arretAuFeuDemande = false;
                        }
                        if(arretAuStopDemande) {
                            arretAuStopDemande = false;
                        }
                    } else if (this.vitesse == 0 && !hitbox.intersects(controleEnCours.getHitbox())) {
                        evaluationMax += 100;
                        evaluation += 100;
                        controleEffectue = true;

                        if(arretAuFeuDemande) {
                            arretAuFeuDemande = false;
                        }
                        if(arretAuStopDemande) {
                            arretAuStopDemande = false;
                        }
                    }
                }

                //si on controle l'absence d'arret au stop on verifie que la vitesse n'est jamais == 0 avant de passer sur le stop
                if(arretAuStopNonDemande || arretAuFeuNonDemande) {
                    if (this.vitesse == 0) {
                        evaluationMax += 100;
                        evaluation -= 100;
                        controleEffectue = true;

                        if(arretAuStopNonDemande) {
                            arretAuStopDemande = false;
                        }
                        if(arretAuFeuNonDemande) {
                            arretAuFeuDemande = false;
                        }
                    } else if (null != calculDistance(controleEnCours) && calculDistance(controleEnCours) > this.getWidth()) {
                        evaluationMax += 100;
                        evaluation += 100;
                        controleEffectue = true;

                        if(arretAuStopNonDemande) {
                            arretAuStopDemande = false;
                        }
                        if(arretAuFeuNonDemande) {
                            arretAuFeuDemande = false;
                        }
                    }
                }
            }
            if(controleEffectue && interactFirst != controleEnCours) {
                controleEnCours = null;
                controleEffectue = false;
            }
        } catch(Exception e) {
            System.out.println("Echec lors de l'évaluation : " + e.getMessage());
        }
    }

    /**
     * selon l'index de direction defini :
     * - l'image du véhicule
     * - l'axe concerné
     * - le sens
     * @param imgIndex
     */
    public void setDirection(int imgIndex) {
        this.imageIndex = imgIndex;
        switch (imgIndex) {
            case 0 :
                this.setFill(imgPatternCarUp);
                setAxeY(true);
                setDirectionSens(-1);
                break;
            case 1 :
                this.setFill(imgPatternCarRight);
                setAxeX(true);
                setDirectionSens(1);
                break;
            case 2 :
                this.setFill(imgPatternCarDown);
                setAxeY(true);
                setDirectionSens(1);
                break;
            case 3 :
                this.setFill(imgPatternCarLeft);
                setAxeX(true);
                setDirectionSens(-1);
                break;
        }
    }

    /**
     * PRIVATES
     */

    private void resetVars() {
        vitesse = 0;
        arretAuStopDemande = false;
        arretAuStopNonDemande = false;
        interactFirst = null;
        controleEnCours = null;
        evaluation = 0;
        evaluationMax = 500;
        controleEffectue = false;
    }

    /**
     * permet de gérer la réaction à l'évènement stop
     * il faut marquer le stop 3 secondes
     * sinon si on est suffisamment du stop on freine pour s'arreter devant
     * sinon on accélère
     * @param evt Evenement
     */
    private void gestionEvtStop(Evenement evt) {
        if(null != this.marquage) {
            long tempsMarque = new Date().getTime()-this.marquage;
            if(tempsMarque >= 3000) {
                this.marquage = null;
                this.arretAuStopEffectue = true;
            }
        }
        if((null == this.arretAuStopEffectue && null != calculDistance(evt) && calculDistance(evt) > this.vitesse && calculDistance(evt) <= calculDistanceFreinage())
                || (null != this.arretAuStopEffectue && !this.arretAuStopEffectue)) {
            this.freiner();
            if(vitesse == 0 && null == this.marquage) {
                this.marquage = new Date().getTime();
                this.arretAuStopEffectue = false;
                Bruitage.sonRouleStop();
            }
        } else {
            this.accelerer();
        }
    }

    /**
     * permet de gérer la réaction à l'évènement feu
     * si le feu est vert : on accélère
     * sinon quand on est a distance on s'arrete devant le feu
     * @param couleurFeuConcerne : variable char correspondant à la première lettre de la couleur du feu en cours
     */
    private void gestionEvtFeu(char couleurFeuConcerne) {
        switch (couleurFeuConcerne) {
            case 'V':
                accelerer();
                arretAuFeu = false;
                break;
            case 'O':
            case 'R':
                if(arretAuFeu || (null != calculDistance(interactFirst) && calculDistance(interactFirst) > this.vitesse && calculDistance(interactFirst) <= calculDistanceFreinage())) {
                    freiner();
                    if(this.vitesse == 0) {
                        arretAuFeu = true;
                        Bruitage.sonRouleStop();
                    }
                } else {
                    accelerer();
                }
                break;
        }
    }

    /**
     * défini si le véhicule est sur le même axe que l'evenement ciblé
     * si on est dans l'axe X
     * le xMax de la cible doit etre > au Xmin du vehicule
     * le xMin de la cible doit etre < au Xmax du vehicule
     * @param cible Evenement : le prochain evenement à interagir
     * @return boolean
     */
    private boolean laCibleEstSurLeMemeAxe(Evenement cible) {
        boolean result;
        if(this.isAxeX()) {
            result = laCibleEstSurLeMemeAxeX(cible);
        } else {
            result = laCibleEstSurLeMemeAxeY(cible);
        }
        return result;
    }
    private boolean laCibleEstSurLeMemeAxeY(Evenement cible) {
        //la cible est sur le même axe Y si le Xmin ou Xmax  du véhicule est compris entre le Xmin et xMax de la cible
        boolean result = false;
        try {
            if(this.getX() == cible.getX()) {
                result = true;
            } else if(this.getX() > cible.getX() && this.getX() < cible.getX() + cible.getWidth()) {
                result = true;
            } else if(this.getX() + this.getWidth() > cible.getX() && this.getX() + this.getWidth() < cible.getX() + cible.getWidth()) {
                result = true;
            }

            return result;
        } catch(Exception e) {
            //System.out.println("Erreur lors du calcul de la distance : " + e.getMessage());
            return false;
        }
    }

    private boolean laCibleEstSurLeMemeAxeX(Evenement cible) {
        //la cible est sur le même axe X si le Ymin ou Ymax  du véhicule est compris entre le Ymin et Ymax de la cible
        boolean result = false;
        try {
            if(this.getY() == cible.getY()) {
                result = true;
            } else if(this.getY() > cible.getY() && this.getY() < cible.getY() + cible.getWidth()) {
                result = true;
            } else if(this.getY() + this.getWidth() > cible.getY() && this.getY() + this.getWidth() < cible.getY() + cible.getWidth()) {
                result = true;
            }

            return result;
        } catch(Exception e) {
            //System.out.println("Erreur lors du calcul de la distance : " + e.getMessage());
            return false;
        }
    }

    private Double calculDistance(Evenement cible) {
        Double distance = null;
        //si on circule dans le même axe on calcul la distance sinon on renvoi null
        if(laCibleEstSurLeMemeAxe(cible)) {
            if(this.isAxeY()) {
                //on calcul la distance entre les points Y d'origines
                distance = Tools.arrondi((cible.getY() - this.getY()) * this.getDirectionSens(), 2);
                if("stop".equals(cible.getType()) || "feu".equals(cible.getType())) {
                    distance -= this.getDirectionSens() > 0 ? this.getHeight() : cible.getHeight();
                }
            } else {
                //on calcul la distance entre les points X d'origines
                distance = Tools.arrondi((cible.getX() - this.getX()) * this.getDirectionSens(), 2);
                if("stop".equals(cible.getType()) || "feu".equals(cible.getType())) {
                    distance -= this.getDirectionSens() > 0 ? this.getWidth() : cible.getWidth();
                }
            }
        }

        return distance;
    }

    private Double calculDistanceFreinage() {
        Double distanceFreinage = null;
        //on calcul le nombre de cycle nécéssaire pour freiner
        //puis on multiplie le nombre de cycle par la durée moyenne d'un cycle (10ms)
        //enfin on multiplie c'ette durée par la vitesse actuelle pour avoir la distance
        //on facorise tout ça :

        return Tools.arrondi((Math.pow(this.vitesse, 2) * 10) / this.freinageMax, 2);
    }

    private double convertirKmHEnPxMs(double vitesseKmH) {
        /**convertir en pixel par heure
            double vitessePxParH = convertirMetreEnPx(vitesseKmH);
        convertir en pixel par milliseconde
            return vitessePxParH / 60*60*1000;
        on factorise*/
        return Tools.arrondi(convertirMetreEnPx(vitesseKmH*1000) / (60*60*1000), 2);
    }

    private double convertirMetreEnPx(double distanceEnMetres) {
        /**on converti en longueur de véhicule
            double nbrLgVehicule = distanceEnMetres/this.longueur;

        on recupere la longueur du véhicule en pixel
        (soit : la largeur/hauteur de l'image qui est carree)
            double longeurVehiculeEnPixel = this.getWidth();

        on fait le rapport
            return nbrLgVehicule * longeurVehiculeEnPixel;

        on factorise*/
        return Tools.arrondi(distanceEnMetres/this.longueur * this.getWidth(), 2);
    }

}
