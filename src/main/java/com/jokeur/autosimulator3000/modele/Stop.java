package com.jokeur.autosimulator3000.modele;

import com.jokeur.autosimulator3000.controlleur.MainScene;
import com.jokeur.autosimulator3000.controlleur.ManipScene;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class Stop extends Evenement {
    private static int indexStop = 0;
    private String statut;

    public Stop (double p_largeurRoute, double xIntersection, double yIntersection, String image) {
        super(p_largeurRoute*1.8, image);
        Stop that = this;

        that.type = "stop";
        if("imgStopY".equals(image)){
            that.statut = "Y";
        }
        else{
            that.statut = "X";
        }

        double largeurStop = p_largeurRoute*1.8; //Taille de l'image
        that.setEvenementX(xIntersection-((largeurStop-p_largeurRoute)/2));
        that.setEvenementY(yIntersection-((largeurStop-p_largeurRoute)/2));
        this.setId(this.type + indexStop);
        indexStop++;

        that.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent g) {
                //on vérifie qu'on est sur la scene principale
                //Stop.DirectionStop(largeurRoute, x, y);
                 if(ManipScene.getSceneActuelle() == 1) {
                    //en fonction de l'action choisie on agit differemment
                    MainScene.creerEvenement(xIntersection, yIntersection);
                }
            }
        });
    }


    public int getIndex(){
        return this.indexStop;
    }
    public String getStatut(){
        return this.statut;
    }
}
