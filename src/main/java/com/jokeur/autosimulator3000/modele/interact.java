package com.jokeur.autosimulator3000.modele;

import javafx.geometry.Rectangle2D;

interface interact {
    String getType();
    Rectangle2D getHitbox();
}
