package com.jokeur.autosimulator3000.modele;

import com.jokeur.autosimulator3000.controlleur.MainScene;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Delete extends Evenement {
    public Delete(double p_largeurRoute) {
        super(p_largeurRoute, "imgDelete");
        Delete that = this;

        this.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if (!MainScene.animationStarted){
                    that.setFill(Color.TRANSPARENT);
                }
            }
        });
    }

}
