package com.jokeur.autosimulator3000.modele;

import com.jokeur.autosimulator3000.aide.Tools;
import com.jokeur.autosimulator3000.controlleur.ConnexionScene;
import com.jokeur.autosimulator3000.controlleur.MainScene;
import com.jokeur.autosimulator3000.controlleur.ManipScene;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.geometry.Rectangle2D;

public abstract class Evenement extends Rectangle implements interact {

    public Image img;
    protected Rectangle2D hitbox;
    protected String type = "motherClass";

    public Evenement(double p_largeurRoute) {
        Evenement that = this;
        that.setHeight(p_largeurRoute);
        that.setWidth(p_largeurRoute);
        makeCollisionsBox();

        this.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if (!MainScene.animationStarted && !"stop".equals(that.getType())){
                    if (t.getButton() == MouseButton.SECONDARY) {
                        MainScene.supprimerEvenementParId(that.getId());
                    }
                }
            }
        });
    }

    public Evenement (double p_largeurRoute, String nomImg){
        this(p_largeurRoute);
        img = new Image(MainScene.class.getResource(Tools.getProperty(nomImg, "ressources.properties")).toExternalForm());
        this.setFill(new ImagePattern(img));
    }

    public Rectangle2D getHitbox(){
        return hitbox;
    }

    private void makeCollisionsBox() {
        hitbox = new Rectangle2D(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    }

    public void setEvenementX(double newX) {
        this.setX(newX);
        makeCollisionsBox();
    }

    public void setEvenementY(double newY) {
        this.setY(newY);
        makeCollisionsBox();
    }

    public String getType() {
        return this.type;
    }

    public String getStatut() {
        return "";
    }
}