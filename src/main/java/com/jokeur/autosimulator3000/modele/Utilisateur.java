package com.jokeur.autosimulator3000.modele;

public class Utilisateur {
    private int id;
    private String pseudo;
    private String prenom;
    private String nom;
    private String email;
    private String mdp;

    public int getId(){
        return id;
    }
    public void setId(int nouvelId){
        id = nouvelId;
    }

    public String getPseudo(){
        return pseudo;
    }
    public void setPseudo(String nouveauPseudo){
        pseudo = nouveauPseudo;
    }

    public String getPrenom(){
        return prenom;
    }
    public void setPrenom(String nouveauPrenom){
        prenom = nouveauPrenom;
    }

    public String getNom(){
        return nom;
    }
    public void setNom(String nouveauNom){
        nom = nouveauNom;
    }

    public String getEmail(){
        return email;
    }
    public void setEmail(String nouveauEmail){
        email = nouveauEmail;
    }

    public String getMdp(){
        return mdp;
    }
    public void setMdp(String nouveauMdp){
        mdp = nouveauMdp;
    }
}