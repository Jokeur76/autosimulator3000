package com.jokeur.autosimulator3000.modele;

public class Right extends Evenement {
    private static int indexRight = 0;

    public Right(double p_largeurRoute) {
        super(p_largeurRoute, "imgRight");
        this.type = "right";
        this.setId(this.type + indexRight);
        indexRight++;
    }

    public int getIndex(){
        return indexRight;
    }
}