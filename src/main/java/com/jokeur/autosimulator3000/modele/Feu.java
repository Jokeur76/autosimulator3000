package com.jokeur.autosimulator3000.modele;

import com.jokeur.autosimulator3000.aide.Tools;
import com.jokeur.autosimulator3000.controlleur.ConnexionScene;
import com.jokeur.autosimulator3000.controlleur.MainScene;
import com.jokeur.autosimulator3000.controlleur.ManipScene;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import java.util.Random;

import java.util.Date;
import java.util.List;

public class Feu extends Evenement {
    //variable statiques

    public static Image imgFeuOR = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgFeuOR", "ressources.properties")).toExternalForm());
    public static ImagePattern imgPatternFeuOR = new ImagePattern(imgFeuOR);
    public static Image imgFeuRO = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgFeuRO", "ressources.properties")).toExternalForm());
    public static ImagePattern imgPatternFeuRO = new ImagePattern(imgFeuRO);
    public static Image imgFeuRR = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgFeuRR", "ressources.properties")).toExternalForm());
    public static ImagePattern imgPatternFeuRR = new ImagePattern(imgFeuRR);
    public static Image imgFeuRV = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgFeuRV", "ressources.properties")).toExternalForm());
    public static ImagePattern imgPatternFeuRV = new ImagePattern(imgFeuRV);
    public static Image imgFeuVR = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgFeuVR", "ressources.properties")).toExternalForm());
    public static ImagePattern imgPatternFeuVR = new ImagePattern(imgFeuVR);

    private String statutFeu;
    private static int indexFeu = 0;
    private int numeroDeFeu;
    private Long debutCompteur = null;

    public Feu(double p_largeurRoute, double x, double y) {
        super(p_largeurRoute * 1.8);

        this.type = "feu";
        this.setId(this.type + indexFeu);
        indexFeu++;

        double largeurFeu = p_largeurRoute * 1.8; //Taille de l'image
        this.setEvenementX(x - ((this.getWidth() - p_largeurRoute) / 2));
        this.setEvenementY(y - ((this.getHeight() - p_largeurRoute) / 2));

        //Génération aléatoire du numéro de statut du feu lors de sa création
        Random rand = new Random();
        numeroDeFeu = rand.nextInt(5 - 0 + 1) + 0;
        setStatutFeu();

        this.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent i) {
                //on vérifie qu'on est sur la scene principale
                //Stop.DirectionStop(largeurRoute, x, y);
                 if(ManipScene.getSceneActuelle() == 1) {
                    //en fonction de l'action choisie on agit differemment
                    MainScene.creerEvenement(x, y);
                }
            }
        });

    }

    public int getIndex() {
        return indexFeu;
    }

    public String getStatut() {
        return statutFeu;
    }

    public void setStatutFeu() {
        switch (numeroDeFeu) {
            case 0:
                this.statutFeu = "VR";
                this.setFill(imgPatternFeuVR);
                break;
            case 1:
                this.statutFeu = "OR";
                this.setFill(imgPatternFeuOR);
                break;
            case 2:
                this.statutFeu = "RR";
                this.setFill(imgPatternFeuRR);
                break;
            case 3:
                this.statutFeu = "RV";
                this.setFill(imgPatternFeuRV);
                break;
            case 4:
                this.statutFeu = "RO";
                this.setFill(imgPatternFeuRO);
                break;
            case 5:
                this.statutFeu = "RR";
                this.setFill(imgPatternFeuRR);
                break;
        }
    }

    public void controles(){
        if (null == debutCompteur){
            debutCompteur = new Date().getTime();
        }

        switch (numeroDeFeu){
            case 0:
                if (new Date().getTime() - this.debutCompteur >= 10000){
                    debutCompteur = null;
                    numeroDeFeu = (numeroDeFeu + 1)%6;
                    setStatutFeu();
                }
                break;
            case 1:
                if (new Date().getTime() - this.debutCompteur >= 3000){
                    debutCompteur = null;
                    numeroDeFeu = (numeroDeFeu + 1)%6;
                    setStatutFeu();
                }
                break;
            case 2:
                if (new Date().getTime() - this.debutCompteur >= 1500){
                    debutCompteur = null;
                    numeroDeFeu = (numeroDeFeu + 1)%6;
                    setStatutFeu();
                }
                break;
            case 3:
                if (new Date().getTime() - this.debutCompteur >= 10000){
                    debutCompteur = null;
                    numeroDeFeu = (numeroDeFeu + 1)%6;
                    setStatutFeu();
                }
                break;
            case 4:
                if (new Date().getTime() - this.debutCompteur >= 3000){
                    debutCompteur = null;
                    numeroDeFeu = (numeroDeFeu + 1)%6;
                    setStatutFeu();
                }
                break;
            case 5:
                if (new Date().getTime() - this.debutCompteur >= 1500){
                    debutCompteur = null;
                    numeroDeFeu = (numeroDeFeu + 1)%6;
                    setStatutFeu();

            }
                break;
        }
    }
}