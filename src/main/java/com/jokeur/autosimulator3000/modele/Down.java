package com.jokeur.autosimulator3000.modele;

public class Down extends Evenement {
    private static int indexDown = 0;

    public Down(double p_largeurRoute) {
        super(p_largeurRoute, "imgDown");
        this.type = "down";
        this.setId(this.type + indexDown);
        System.out.println(getId());
        indexDown++;

    }

    public int getIndex(){
        return indexDown;
    }
}