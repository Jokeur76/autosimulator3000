package com.jokeur.autosimulator3000.modele.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

import com.jokeur.autosimulator3000.controlleur.ManipScene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import com.jokeur.autosimulator3000.modele.Utilisateur;

public class DAOUtilisateur {

    public static void setUtilisateur(Utilisateur nouvelUtilisateur, Stage stage) throws Exception {
        Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;
        boolean insertionOk = false;

        connexion = DAOConnexion.ouvrir();

        try {

            statement = connexion.createStatement();
            insertionOk = statement.execute("INSERT INTO t_utilisateur (pseudo, nom, prenom, email, mdp) VALUES ('"+nouvelUtilisateur.getPseudo()+"', '"+nouvelUtilisateur.getNom()+"', '"+nouvelUtilisateur.getPrenom()+"', '"+nouvelUtilisateur.getEmail()+"', '"+nouvelUtilisateur.getMdp()+"')");
            //Button btnConnexion = (Button) stage.getScene().lookup("#btnConnexion");  
        }
        catch (SQLException sqlEx){
            // handle any errors
            System.out.println("SQLException: " + sqlEx.getMessage());
            System.out.println("SQLState: " + sqlEx.getSQLState());
            System.out.println("VendorError: " + sqlEx.getErrorCode());
        }
        catch(Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
        }
        finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException sqlEx) { } // ignore
                statement = null;
            }
            DAOConnexion.fermer(connexion);
        }
    }

    public static void updateUtilisateur(Utilisateur utilisateurMaj) throws Exception {
        Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;
        boolean updateOk = false;

        connexion = DAOConnexion.ouvrir();

        try {

            statement = connexion.createStatement();
            updateOk = statement.execute("UPDATE t_utilisateur SET pseudo = '"+utilisateurMaj.getPseudo()+"', prenom = '"+utilisateurMaj.getPrenom()+"', nom ='"+utilisateurMaj.getNom()+"', email = '"+utilisateurMaj.getEmail()+"', mdp = "+utilisateurMaj.getMdp()+" WHERE id = ('"+utilisateurMaj.getId()+"')");

        }
        catch (SQLException sqlEx){
            // handle any errors
            System.out.println("SQLException: " + sqlEx.getMessage());
            System.out.println("SQLState: " + sqlEx.getSQLState());
            System.out.println("VendorError: " + sqlEx.getErrorCode());
        }
        catch(Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
        }
        finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException sqlEx) { } // ignore
                statement = null;
            }
            DAOConnexion.fermer(connexion);
        }
    }

    public static Utilisateur getUtilisateurByPseudo(String pseudoCherche) throws Exception {

        Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;
        boolean insertionOk = false;
        Utilisateur utilisateur = new Utilisateur();

        connexion = DAOConnexion.ouvrir();

        try {

            statement = connexion.createStatement();
            resultat = statement.executeQuery("SELECT * FROM t_utilisateur WHERE pseudo = '"+pseudoCherche+"'");
            resultat.next();
            utilisateur.setId(resultat.getInt("id"));
            utilisateur.setPseudo(resultat.getString("pseudo"));
            utilisateur.setPrenom(resultat.getString("prenom"));
            utilisateur.setNom(resultat.getString("nom"));
            utilisateur.setEmail(resultat.getString("email"));
            utilisateur.setMdp(resultat.getString("mdp"));

        }
        catch (SQLException ex){
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException sqlEx) { } // ignore
                statement = null;
            }
            DAOConnexion.fermer(connexion);
        }
        return utilisateur;
    }

    /*Fonction qui vérifie si le pseudo entrée en paramètre existe en base de donnée.
     * Si il existe, elle renvoie True sinon False */
    public static boolean verifPseudo(String pseudo) throws Exception {
        Utilisateur u = new Utilisateur();
        boolean pseudoExiste = true;

        u = getUtilisateurByPseudo(pseudo);
        if(u.getPseudo() == null){
            pseudoExiste = false;
        }
        return pseudoExiste;
    }

    public static Utilisateur getUtilisateurByMdp(String mdpCherche) throws Exception {

        Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;
        boolean insertionOk = false;
        Utilisateur utilisateur = new Utilisateur();

        connexion = DAOConnexion.ouvrir();

        try {

            statement = connexion.createStatement();
            resultat = statement.executeQuery("SELECT * FROM t_utilisateur WHERE mdp = '"+mdpCherche+"'");
            resultat.next();
            utilisateur.setPseudo(resultat.getString("pseudo"));
            utilisateur.setPrenom(resultat.getString("prenom"));
            utilisateur.setNom(resultat.getString("nom"));
            utilisateur.setEmail(resultat.getString("email"));
            utilisateur.setMdp(resultat.getString("mdp"));

        }
        catch (SQLException ex){
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException sqlEx) { } // ignore
                statement = null;
            }
            DAOConnexion.fermer(connexion);
        }
        return utilisateur;
    }

    public static boolean verifMdp(String mdp) throws Exception {
        Utilisateur u = new Utilisateur();
        boolean mdpExiste = true;

        u = getUtilisateurByMdp(mdp);
        if(u.getMdp() == null){
            mdpExiste = false;
        }
        return mdpExiste;
    }
}
