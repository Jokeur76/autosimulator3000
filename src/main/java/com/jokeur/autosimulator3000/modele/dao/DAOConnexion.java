package com.jokeur.autosimulator3000.modele.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

//classes du projet
import com.jokeur.autosimulator3000.aide.Tools;

public class DAOConnexion {
    public static Connection ouvrir() throws Exception {
        Connection connexion = null;
        String url = "jdbc:mysql://localhost:3306/" + Tools.getProperty("bdd") + "?autoReconnect=true&useSSL=false";
        String username = Tools.getProperty("user");
        String password = Tools.getProperty("password");

        //Ouverture de la connexion
        try {
            connexion = DriverManager.getConnection(url, username, password);
            return connexion;
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }
    }
    public static void fermer(Connection connexion){
        try {
        connexion.close();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    public static void testConnection() {
        loadDriver();
        String url = "jdbc:mysql://localhost:3306/" + Tools.getProperty("bdd") + "?autoReconnect=true&useSSL=false";
        String username = Tools.getProperty("user");
        String password = Tools.getProperty("password");

        System.out.println("Connecting database...");

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            System.out.println("Database connected!");
        } catch (SQLException e) {
            throw new IllegalStateException("Cannot connect the database!", e);
        }
    }

    public static void loadDriver() {
        System.out.println("Loading driver...");

        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Driver loaded!");
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Cannot find the driver in the classpath!", e);
        }
    }
}