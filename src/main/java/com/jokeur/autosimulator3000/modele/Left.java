package com.jokeur.autosimulator3000.modele;

public class Left extends Evenement {
    private static int indexLeft = 0;

    public Left(double p_largeurRoute) {
        super(p_largeurRoute, "imgLeft");
        this.type = "left";
        this.setId(this.type + indexLeft);
        indexLeft++;
    }

    public int getIndex(){
        return indexLeft;
    }
}
