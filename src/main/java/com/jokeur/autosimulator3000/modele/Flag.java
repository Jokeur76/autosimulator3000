package com.jokeur.autosimulator3000.modele;

public class Flag extends Evenement {
    private static int indexFlag = 0;

    public Flag(double p_largeurRoute) {
        super(p_largeurRoute, "imgFlag");
        this.type = "flag";
        this.setId(this.type + indexFlag);
        indexFlag++;
    }

    public int getIndex(){
        return indexFlag;
    }

}
