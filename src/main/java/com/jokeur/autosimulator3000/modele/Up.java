package com.jokeur.autosimulator3000.modele;

public class Up extends Evenement {
    private static int indexUp = 0;

    public Up(double p_largeurRoute) {
        super(p_largeurRoute, "imgUp");
        this.type = "up";
        this.setId(this.type + indexUp);
        indexUp++;
    }

    public int getIndex(){
        return indexUp;
    }
}