package com.jokeur.autosimulator3000.controlleur;

import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import com.jokeur.autosimulator3000.modele.*;
import com.jokeur.autosimulator3000.modele.dao.DAOUtilisateur;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Spinner;
import javafx.scene.control.ComboBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.CheckBox;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.text.FontWeight;
import com.jokeur.autosimulator3000.controlleur.ManipScene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Region;
import javafx.scene.control.Label;
import com.jokeur.autosimulator3000.controlleur.Bruitage;

//classe du projet
import com.jokeur.autosimulator3000.aide.Tools;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;


public class MainScene {
    private static Double tailleFont30;
    private static Double tailleFont16;

    private static Group conteneurCarte = new Group();
    public static double largeurCadreCarte;
    public static double hauteurCadreCarte;
    public static boolean animationStarted = false;

    public static double padding;
    public static double largeurCadreProfil;
    public static double hauteurCadreProfil;
    public static Group profil = new Group();
    public static Group grpFormInscription = new Group();
    public static Group grpLbFormInscription = new Group();

    public static TextField textboxPseudo = new TextField();
    public static TextField textboxNom = new TextField();
    public static TextField textboxPrenom = new TextField();
    public static TextField textboxMail = new TextField();
    public static TextField textboxMdp = new TextField();

    private static Button btnStart;

    public static Integer action;

    public static Vehicule vehicule = null;
    public static Flag Flag = null;
    public static Up up = null;
    public static Right right = null;
    public static Down down = null;
    public static Left Left = null;
    public static Delete delete = null;
    public static Evenement direction = null;

    public static Rectangle2D carteBox;

    public static List<Evenement> listeEvenement = new ArrayList<>();

    public static Image imgFlag = new Image(MainScene.class.getResource(Tools.getProperty("imgFlag", "ressources.properties")).toExternalForm());
    public static Image imgCar = new Image(MainScene.class.getResource(Tools.getProperty("imgCar", "ressources.properties")).toExternalForm());
    public static Image imgUp = new Image(MainScene.class.getResource(Tools.getProperty("imgUp", "ressources.properties")).toExternalForm());
    public static Image imgDown = new Image(MainScene.class.getResource(Tools.getProperty("imgDown", "ressources.properties")).toExternalForm());
    public static Image imgLeft = new Image(MainScene.class.getResource(Tools.getProperty("imgLeft", "ressources.properties")).toExternalForm());
    public static Image imgRight = new Image(MainScene.class.getResource(Tools.getProperty("imgRight", "ressources.properties")).toExternalForm());
    public static ImagePattern imgPatternFlag = new ImagePattern(imgFlag);
    public static ImagePattern imgPatternCar = new ImagePattern(imgCar);
    public static ImagePattern imgPatternUp = new ImagePattern(imgUp);
    public static ImagePattern imgPatternDown = new ImagePattern(imgDown);
    public static ImagePattern imgPatternLeft = new ImagePattern(imgLeft);
    public static ImagePattern imgPatternRight = new ImagePattern(imgRight);

    public static int vitesseSelec = 50;

    /**
     * retourne la scene principale de l'application
     *
     * @return
     */
    public static Group getRoot(Stage stage) {
        //double padding = stage.getHeight() * 0.03;
        tailleFont30 = stage.getHeight() * 0.03;
        tailleFont16 = stage.getHeight() * 0.016;
        //TEST ICON
        //Recuperation des images avec URL

        Image imgDelete = new Image(MainScene.class.getResource(Tools.getProperty("imgDelete", "ressources.properties")).toExternalForm());
        Image imgDeco = new Image(MainScene.class.getResource(Tools.getProperty("imgDeco", "ressources.properties")).toExternalForm());
        Image imgFond = new Image(MainScene.class.getResource(Tools.getProperty("imgFond", "ressources.properties")).toExternalForm());
        Image imgFondParam = new Image(MainScene.class.getResource(Tools.getProperty("imgFondParam", "ressources.properties")).toExternalForm());

        Group root = new Group();

        //cadre Paramètre
        double largeurCadreParams = stage.getWidth() * 0.2;
        double hauteurCadreParams = stage.getHeight() * 0.5;
        Rectangle cadreParams = new Rectangle(0, 0, largeurCadreParams, hauteurCadreParams);
        cadreParams.setFill(Color.GREY);
        cadreParams.setStyle(
                "-fx-stroke: black; "
                        + "-fx-stroke-size: 2;"
        );
        cadreParams.setId("cadreParams");

        // Cadre Profil
        padding = stage.getHeight() * 0.02;
        largeurCadreProfil = stage.getWidth() * 0.2;
        hauteurCadreProfil = stage.getHeight() - (padding * 2);
        Rectangle cadreProfil = new Rectangle(padding, padding, largeurCadreProfil, hauteurCadreProfil);
        cadreProfil.setFill(Color.GRAY);
        cadreProfil.setStyle(
                "-fx-stroke: black; "
                        + "-fx-stroke-size: 2;"
        );


        String titreMenu = "Bienvenue";

            //Labels
        Text lbTitreMenu = new Text(largeurCadreProfil*0.17,hauteurCadreProfil*0.07,titreMenu);
        lbTitreMenu.setFont(Font.font("Tahoma", FontWeight.BOLD, 23));
        lbTitreMenu.setFill(Color.WHITE);
        Text actualPseudo = new Text(largeurCadreProfil*0.17, hauteurCadreProfil*0.12, "Pseudo:");
        actualPseudo.setFont(Font.font("Tahoma", FontWeight.BOLD, 13));
        actualPseudo.setFill(Color.WHITE);
        Text actualPrenom = new Text(largeurCadreProfil*0.17, hauteurCadreProfil*0.22, "Prenom:");
        actualPrenom.setFont(Font.font("Tahoma", FontWeight.BOLD, 13));
        actualPrenom.setFill(Color.WHITE);
        Text actualNom = new Text(largeurCadreProfil*0.17, hauteurCadreProfil*0.32, "Nom:");
        actualNom.setFont(Font.font("Tahoma", FontWeight.BOLD, 13));
        actualNom.setFill(Color.WHITE);
        Text actualMail = new Text(largeurCadreProfil*0.17, hauteurCadreProfil*0.42, "Email:");
        actualMail.setFont(Font.font("Tahoma", FontWeight.BOLD, 13));
        actualMail.setFill(Color.WHITE);
        Text actualMdp = new Text(largeurCadreProfil*0.17, hauteurCadreProfil*0.52, "Mot de passe:");
        actualMdp.setFont(Font.font("Tahoma", FontWeight.BOLD, 13));
        actualMdp.setFill(Color.WHITE);

        //groupeInscription.getChildren().add(lbTitreInscription);
        grpFormInscription.getChildren().add(lbTitreMenu);
        grpFormInscription.getChildren().add(actualPseudo);
        grpFormInscription.getChildren().add(actualNom);
        grpFormInscription.getChildren().add(actualPrenom);
        grpFormInscription.getChildren().add(actualMail);
        grpFormInscription.getChildren().add(actualMdp);
            //TextBox
        TextField textboxPseudo = new TextField();
        grpLbFormInscription.getChildren().add(textboxPseudo);
        textboxPseudo.setTranslateX(largeurCadreProfil*0.17);
        textboxPseudo.setTranslateY(hauteurCadreProfil*0.13);
        textboxPseudo.setMinWidth(largeurCadreProfil*0.75);
        textboxPseudo.setDisable(true);
        TextField textboxPrenom = new TextField();
        grpLbFormInscription.getChildren().add(textboxPrenom);
        textboxPrenom.setTranslateX(largeurCadreProfil*0.17);
        textboxPrenom.setTranslateY(hauteurCadreProfil*0.23);
        textboxPrenom.setMinWidth(largeurCadreProfil*0.75);
        textboxPrenom.setDisable(true);
        TextField textboxNom = new TextField();
        grpLbFormInscription.getChildren().add(textboxNom);
        textboxNom.setTranslateX(largeurCadreProfil*0.17);
        textboxNom.setTranslateY(hauteurCadreProfil*0.33);
        textboxNom.setMinWidth(largeurCadreProfil*0.75);
        textboxNom.setDisable(true);
        TextField textboxMail = new TextField();
        grpLbFormInscription.getChildren().add(textboxMail);
        textboxMail.setTranslateX(largeurCadreProfil*0.17);
        textboxMail.setTranslateY(hauteurCadreProfil*0.43);
        textboxMail.setMinWidth(largeurCadreProfil*0.75);
        textboxMail.setDisable(true);
        PasswordField  textboxMdp = new PasswordField ();
        grpLbFormInscription.getChildren().add(textboxMdp);
        textboxMdp.setTranslateX(largeurCadreProfil*0.17);
        textboxMdp.setTranslateY(hauteurCadreProfil*0.53);
        textboxMdp.setMinWidth(largeurCadreProfil*0.75);
        textboxMdp.setDisable(true);
        textboxMdp.setVisible(true);
        TextField textboxMdp2 = new TextField();
        grpLbFormInscription.getChildren().add(textboxMdp2);
        textboxMdp2.setTranslateX(largeurCadreProfil*0.17);
        textboxMdp2.setTranslateY(hauteurCadreProfil*0.53);
        textboxMdp2.setMinWidth(largeurCadreProfil*0.75);
        textboxMdp2.setDisable(true);
        textboxMdp2.setVisible(false);

        //Boutons
        Button editProfil = new Button("Editer");
        //grpLbFormInscription.getChildren().add(editProfil);
        editProfil.setDefaultButton(true);
        editProfil.setMinWidth(largeurCadreProfil*0.3);
        editProfil.setTranslateX(largeurCadreProfil*0.17);
        editProfil.setTranslateY(hauteurCadreProfil*0.61);

        textboxMdp.addEventHandler(MouseEvent.MOUSE_ENTERED_TARGET,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
              textboxMdp.setDisable(true);
              textboxMdp.setVisible(false);
              textboxMdp2.setVisible(true);
              textboxMdp2.requestFocus();
              textboxMdp2.setDisable(false);
              textboxMdp2.setText(textboxMdp.getCharacters().toString());
            }
        });
        textboxMdp2.addEventHandler(MouseEvent.MOUSE_EXITED,
            new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
              textboxMdp.setDisable(false);
              textboxMdp.setVisible(true);
              textboxMdp.requestFocus();
              textboxMdp2.setVisible(false);
              textboxMdp2.setDisable(true);
              textboxMdp.setText(textboxMdp2.getCharacters().toString());
            }
        });

        Button saveProfil = new Button("Valider");
        //grpLbFormInscription.getChildren().add(saveProfil);
        saveProfil.setDefaultButton(true);
        saveProfil.setMinWidth(largeurCadreProfil*0.3);
        saveProfil.setTranslateX(largeurCadreProfil*0.17);
        saveProfil.setTranslateY(hauteurCadreProfil*0.61);
        saveProfil.setVisible(false);

        Button annulProfil = new Button("Annuler");
        //grpLbFormInscription.getChildren().add(saveProfil);
        annulProfil.setDefaultButton(true);
        annulProfil.setMinWidth(largeurCadreProfil*0.3);
        annulProfil.setTranslateX(largeurCadreProfil*0.62);
        annulProfil.setTranslateY(hauteurCadreProfil*0.61);
        annulProfil.setVisible(false);

        Button deconnection = new Button("Deconnexion");
        double decoHeight = ManipScene.getScreenHeight() * 0.02;
        deconnection.setTranslateX(largeurCadreProfil*0.45);
        deconnection.setTranslateY(hauteurCadreProfil*0.95);

        Utilisateur userToRead = ManipScene.getActiveUser();
        String getPseudo = userToRead.getPseudo();
        String getPrenom = userToRead.getPrenom();
        String getNom = userToRead.getNom();
        String getEmail = userToRead.getEmail();
        String getMdp = userToRead.getMdp();
        textboxPseudo.setText(getPseudo);
        textboxPrenom.setText(getPrenom);
        textboxNom.setText(getNom);
        textboxMail.setText(getEmail);
        textboxMdp.setText(getMdp);

        editProfil.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent actionEvent) {
              String titreMenu = "Modifiez votre profil";
              textboxPseudo.setDisable(false);
              textboxPrenom.setDisable(false);
              textboxNom.setDisable(false);
              textboxMail.setDisable(false);
              editProfil.setVisible(false);
              saveProfil.setVisible(true);
              annulProfil.setVisible(true);
              textboxMdp.setDisable(false);

                //editProfil(true);
              Utilisateur userToRead = ManipScene.getActiveUser();
              String getPseudo = userToRead.getPseudo();
              String getPrenom = userToRead.getPrenom();
              String getNom = userToRead.getNom();
              String getEmail = userToRead.getEmail();
              String getMdp = userToRead.getMdp();
              textboxPseudo.setText(getPseudo);
              textboxPrenom.setText(getPrenom);
              textboxNom.setText(getNom);
              textboxMail.setText(getEmail);
              textboxMdp.setText(getMdp);
            }
        });

        saveProfil.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent actionEvent) {
              editProfil.setVisible(true);
              saveProfil.setVisible(false);
              annulProfil.setVisible(false);
              textboxPseudo.setDisable(true);
              textboxPrenom.setDisable(true);
              textboxNom.setDisable(true);
              textboxMail.setDisable(true);
              textboxMdp.setDisable(true);
              textboxMdp.setVisible(true);
              //textboxMdp2.setVisible(true);

              String titreMenu = "Bienvenue";
                //editProfil(false);
              Utilisateur userToUpdate = ManipScene.getActiveUser();
              userToUpdate.setPseudo(textboxPseudo.getCharacters().toString());
              userToUpdate.setPrenom(textboxPrenom.getCharacters().toString());
              userToUpdate.setNom(textboxNom.getCharacters().toString());
              userToUpdate.setEmail(textboxMail.getCharacters().toString());
              userToUpdate.setMdp(textboxMdp.getCharacters().toString());
              Utilisateur userToRead = ManipScene.getActiveUser();
              String getMdp = userToRead.getMdp();
              textboxMdp.setText(getMdp);

              try {
                  DAOUtilisateur.updateUtilisateur(userToUpdate);
              } catch (Exception z) {
                  System.out.println("Erreur lors de la deconnexion : " + z.getMessage());
              }
            }
        });

        annulProfil.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent actionEvent) {
              editProfil.setVisible(true);
              saveProfil.setVisible(false);
              annulProfil.setVisible(false);
              textboxPseudo.setDisable(true);
              textboxPrenom.setDisable(true);
              textboxNom.setDisable(true);
              textboxMail.setDisable(true);
              textboxMdp.setDisable(true);
              textboxMdp.setVisible(true);
              //textboxMdp2.setVisible(false);
              String titreMenu = "Bienvenue";

              Utilisateur userToRead = ManipScene.getActiveUser();
              String getPseudo = userToRead.getPseudo();
              String getPrenom = userToRead.getPrenom();
              String getNom = userToRead.getNom();
              String getEmail = userToRead.getEmail();
              String getMdp = userToRead.getMdp();
              textboxPseudo.setText(getPseudo);
              textboxPrenom.setText(getPrenom);
              textboxNom.setText(getNom);
              textboxMail.setText(getEmail);
              textboxMdp.setText(getMdp);
            }
        });

        ////////ICON//////////
        Group boutonIcon = new Group();
        boutonIcon.setTranslateX((stage.getWidth() - 350) * 0.5);
        boutonIcon.setTranslateY(hauteurCadreProfil * 0.90);

        //1er bouton a 0 du bord
        Button buttonCar = new Button();
        ImageView iconCar = new ImageView(imgCar);
        iconCar.setFitWidth(50);
        iconCar.setFitHeight(50);
        buttonCar.setGraphic(iconCar);

        //2eme bouton a 70 du bord
        Button buttonFlag = new Button();
        ImageView iconFlag = new ImageView(imgFlag);
        iconFlag.setFitWidth(50);
        iconFlag.setFitHeight(50);
        buttonFlag.setTranslateX(70);
        buttonFlag.setGraphic(iconFlag);

        //3eme bouton a 140 du bord
        Button buttonUp = new Button();
        ImageView iconUp = new ImageView(imgUp);
        iconUp.setFitWidth(50);
        iconUp.setFitHeight(50);
        buttonUp.setTranslateX(140);
        buttonUp.setGraphic(iconUp);

        //4eme bouton a 210 du bord
        Button buttonRight = new Button();
        ImageView iconRight = new ImageView(imgRight);
        iconRight.setFitWidth(50);
        iconRight.setFitHeight(50);
        buttonRight.setTranslateX(210);
        buttonRight.setGraphic(iconRight);

        //5eme bouton a 280 du bord
        Button buttonDown = new Button();
        ImageView iconDown = new ImageView(imgDown);
        iconDown.setFitWidth(50);
        iconDown.setFitHeight(50);
        buttonDown.setTranslateX(280);
        buttonDown.setGraphic(iconDown);

        //6eme bouton a 350 du bord
        Button buttonLeft = new Button();
        ImageView iconLeft = new ImageView(imgLeft);
        iconLeft.setFitWidth(50);
        iconLeft.setFitHeight(50);
        buttonLeft.setTranslateX(350);
        buttonLeft.setGraphic(iconLeft);

        //6eme bouton a 440 du bord
        Button buttonDelete = new Button();
        ImageView iconDelete = new ImageView(imgDelete);
        iconDelete.setFitWidth(50);
        iconDelete.setFitHeight(50);
        buttonDelete.setTranslateX(440);
        buttonDelete.setGraphic(iconDelete);

        boutonIcon.getChildren().add(buttonCar);
        boutonIcon.getChildren().add(buttonFlag);
        boutonIcon.getChildren().add(buttonUp);
        boutonIcon.getChildren().add(buttonRight);
        boutonIcon.getChildren().add(buttonDown);
        boutonIcon.getChildren().add(buttonLeft);
        boutonIcon.getChildren().add(buttonDelete);

        //largeurCadreProfil = stage.getWidth() * 0.2;
        //hauteurCadreProfil = stage.getHeight() - (padding * 2);

        Rectangle evenementVisuel = new Rectangle(largeurCadreProfil*0.35, hauteurCadreProfil*0.75, largeurCadreProfil*0.4, largeurCadreProfil*0.4);
        evenementVisuel.setFill(Color.GREY);
        grpFormInscription.getChildren().add(evenementVisuel);

        ////////CADRE PARAMETRES//////////
        Text titre = new Text(10, 40, "Paramètres du véhicule");
        titre.setFont(new Font(tailleFont30.intValue()));
        titre.setFill(Color.WHITE);
        titre.setStyle(
                "-fx-background-color: grey; "
        );

        //poids,
        Text labelVitesse = new Text(0, 0, "Vitesse Max :");
        labelVitesse.setFont(new Font(tailleFont16.intValue()));
        labelVitesse.setFill(Color.BLACK);

        Spinner champVitesse = new Spinner(50, 150, 50, 10);
        champVitesse.setId("champVitesse");
        champVitesse.setTranslateX(labelVitesse.getLayoutBounds().getWidth() + 10);
        champVitesse.setTranslateY(-labelVitesse.getLayoutBounds().getHeight());

        Group poids = new Group();
        poids.setTranslateY(80 + labelVitesse.getLayoutBounds().getHeight());
        poids.setTranslateX(10);
        poids.getChildren().add(labelVitesse);
        poids.getChildren().add(champVitesse);

        //couleur
        Text labelCouleur = new Text(0, 0, "Couleur :");
        labelCouleur.setFont(new Font(tailleFont16.intValue()));
        labelCouleur.setFill(Color.BLACK);

        ComboBox boxCouleur = new ComboBox(Tools.getJavaFXColors());
        boxCouleur.getSelectionModel().selectFirst();
        boxCouleur.setTranslateX(labelCouleur.getLayoutBounds().getWidth() + 10);
        boxCouleur.setTranslateY(-labelCouleur.getLayoutBounds().getHeight());

        Button validerParams = new Button("Valider");
        validerParams.setTranslateY(30);

        Group couleur = new Group();
        couleur.setTranslateY(150);
        couleur.setTranslateX(10);
        couleur.getChildren().add(labelCouleur);
        couleur.getChildren().add(boxCouleur);
        couleur.getChildren().add(validerParams);

        //Group profil = new Group();
        profil.getChildren().add(cadreProfil);
        profil.setTranslateX(0);
        profil.setTranslateY(0);

        Group paramsVehicule = new Group();
        paramsVehicule.setId("paramsVehicule");
        paramsVehicule.setTranslateX(stage.getWidth() - largeurCadreParams - padding);
        paramsVehicule.setTranslateY(stage.getHeight() * 0.5 - (hauteurCadreParams * 0.5));
        paramsVehicule.getChildren().add(cadreParams);
        paramsVehicule.getChildren().add(titre);
        paramsVehicule.getChildren().add(poids);
        paramsVehicule.getChildren().add(couleur);
        paramsVehicule.setVisible(false);

        ///////////BOUTON START///////////////////////
        btnStart = new Button("START");
        btnStart.setTranslateY(hauteurCadreProfil * 0.90);
        btnStart.setTranslateX(stage.getWidth() * 0.84);

        btnStart.setStyle(
                "-fx-border-color: lightblue; "
                        + "-fx-font-size: 30;"
                        + "-fx-border-insets: -5; "
                        + "-fx-border-radius: 5;"
                        + "-fx-border-style: solid;"
                        + "-fx-border-width: 2;"
        );

        ///////////APPELLE PARENTS////////////////
        //cadre carte
        largeurCadreCarte = stage.getWidth() - largeurCadreParams - largeurCadreProfil - 2 * padding;
        hauteurCadreCarte = stage.getHeight() * 0.7;
        conteneurCarte = new Group();
        conteneurCarte.setTranslateX(largeurCadreProfil + padding);
        conteneurCarte.setTranslateY(stage.getHeight() * 0.5 - (hauteurCadreCarte * 0.5));

        carteBox = new Rectangle2D(0, 0, largeurCadreCarte, hauteurCadreCarte);
        /*Rectangle radarVisible = new Rectangle(0, 0, largeurCadreCarte, hauteurCadreCarte);
        radarVisible.setVisible(false);
        radarVisible.setStyle(
                "-fx-border-color: lightblue; "
                        + "-fx-font-size: 16;"
                        + "-fx-border-insets: -5; "
                        + "-fx-border-radius: 5;"
                        + "-fx-border-style: dotted;"
                        + "-fx-border-width: 2;"
        );
        radarVisible.setId("radarVisible");
        radarVisible.setFill(Color.BEIGE);
        conteneurCarte.getChildren().add(radarVisible);*/

        conteneurCarte.getChildren().add(Tools.genererCarteDefaut(largeurCadreCarte, hauteurCadreCarte, true));
        conteneurCarte.setId("conteneurCarte");

        profil.getChildren().add(grpFormInscription);
        profil.getChildren().add(grpLbFormInscription);
        root.getChildren().add(profil);
        root.getChildren().add(saveProfil);
        root.getChildren().add(editProfil);
        root.getChildren().add(annulProfil);
        root.getChildren().add(conteneurCarte);
        root.getChildren().add(paramsVehicule);
        root.getChildren().add(boutonIcon);
        root.getChildren().add(deconnection);
        root.getChildren().add(btnStart);
        root.getChildren().add(Musique.addMuteButton());

        deconnection.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent actionEvent) {
              Utilisateur userToUpdate = ManipScene.getActiveUser();
                try {
                    resetMainScene();
                    ManipScene.nopActiveUser(DAOUtilisateur.getUtilisateurByPseudo(userToUpdate.getPseudo()));
                    ManipScene.setConnexionScene(stage);
                } catch (Exception e) {
                    System.out.println("Erreur lors de la deconnexion : " + e.getMessage());
                }
            }
        });

        buttonCar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                evenementVisuel.setFill(imgPatternCar);
                action = 1;
            }
        });

        buttonFlag.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                evenementVisuel.setFill(imgPatternFlag);
                action = 2;
            }
        });

        buttonUp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                direction = null;
                evenementVisuel.setFill(imgPatternUp);
                action = 3;
            }
        });

        buttonRight.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                direction = null;
                evenementVisuel.setFill(imgPatternRight);
                action = 4;
            }
        });

        buttonDown.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                direction = null;
                evenementVisuel.setFill(imgPatternDown);
                action = 5;
            }
        });

        buttonLeft.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                direction = null;
                evenementVisuel.setFill(imgPatternLeft);
                action = 6;
            }
            /*
            this.setOnMouseClicked(new EventHandler<MouseEvent>(){
                if (!MainScene.animationStarted){
                  if (t.getButton() == MouseButton.SECONDARY) {
                    listeEvenement.remove(this);
                    conteneurCarte.getChildren().remove(this);
                    evenementVisuel.setFill(null);
                    action = 0;
                  }
                }
            }
            */
        });

        buttonDelete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void
            handle(ActionEvent actionEvent) {
                reset();
            }
        });

        btnStart.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                switchAnimation();
            }
        });


        validerParams.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {

               String vitesseMax = champVitesse.getEditor().getText();
               int vitesseSelec = Integer.parseInt(vitesseMax);
               System.out.println(vitesseSelec);
               if (null != vehicule){
                  vehicule.setVitesseMax(vitesseSelec);
               }else{
                  Alert alert = new Alert(Alert.AlertType.NONE, "Vous n'avez pas créé de véhicule.", ButtonType.FINISH);
                  alert.getDialogPane().getChildren().stream().filter(node -> node instanceof Label).forEach(node -> ((Label)node).setMinHeight(Region.USE_PREF_SIZE));
                  alert.show();
               }

            }
        });

        return root;
    }

    public static void mainAnimation(Stage stage) {
        if (animationStarted){
            if (vehicule != null) {
                vehicule.controles();
            }

            //On parcourt la liste d'evenements. Si l'evenement est un feu, on appelle la fonction "controles" de l'objet feu
            for (Evenement evt : listeEvenement){
                if ("feu".equals(evt.getType()) && animationStarted){
                    ((Feu)evt).controles();
                }
            }

        }
        if (vehicule != null && animationStarted) {
            vehicule.controles();
        }
    }

    public static void switchAnimation(){
        if(null != vehicule) {
            Bruitage.sonKlaxon();
            vehicule.toFront();
            animationStarted = !animationStarted;
            if (animationStarted) {
                btnStart.setText("STOP");
            } else {
                btnStart.setText("START");
                Bruitage.sonRouleStop();
            }
        }
    }
    public static void reset(){
        try {
            resetMainScene();
            ManipScene.setMainScene(ManipScene.getStage());
        }
        catch (Exception e){
            System.out.println("Erreur lors du rechargement de la scene : " + e);
        }

    }

    public static void resetMainScene() {
        conteneurCarte = new Group();
        animationStarted = false;
        profil = new Group();
        grpFormInscription = new Group();
        grpLbFormInscription = new Group();

        textboxPseudo = new TextField();
        textboxNom = new TextField();
        textboxPrenom = new TextField();
        textboxMail = new TextField();
        textboxMdp = new TextField();

        action = null;
        vehicule = null;
        Flag = null;
        direction = null;
        delete = null;
        carteBox = null;

        listeEvenement = new ArrayList<>();
    }

    public static void supprimerEvenementParId(String idEvt) {
        conteneurCarte.getChildren().remove(ManipScene.getScene().lookup("#"+idEvt));
        if(idEvt.length() >= 8 && "vehicule".equals(idEvt.substring(0, 8))) {
            vehicule = null;
            Group paramsVehicule = (Group) ManipScene.getScene().lookup("#paramsVehicule");
            paramsVehicule.setVisible(false);
        } else {
            for(Evenement evt : listeEvenement) {
                if(idEvt.equals(evt.getId())) {
                    listeEvenement.remove(evt);
                    break;
                }
            }
            if(idEvt.length() >= 4 && "flag".equals(idEvt.substring(0, 4))) {
                Flag = null;
            } else {
                direction = null;
            }
        }
    }

    private static void remplirChampsProfil() {
        //String test = ManipScene.getStage().getScene().lookup("#actualPseudo");
        //Text test = (Text) ManipScene.getStage().getScene().lookup("#actualPseudo");
        //System.out.println("test:" + test.getText());
    }

    public static Node creerEvenementCarteAuto(double xIntersection, double yIntersection, double dimension, String index){
        switch(index) {
            case "stopX":
                Stop stopX = new Stop(dimension, xIntersection, yIntersection ,"imgStopX");
                listeEvenement.add(stopX);
                return stopX;
            case "stopY":
                Stop stopY = new Stop(dimension, xIntersection, yIntersection ,"imgStopY");
                listeEvenement.add(stopY);
                return stopY;
            case "feu":
                Feu feu = new Feu(dimension, xIntersection, yIntersection);
                listeEvenement.add(feu);
                return feu;
            default:
                return null;
        }
    }

    public static void creerEvenement(double X, double Y){
        if(null != action) {
            switch (action) {
                case 1:
                    //placement du vehicule
                    //on verifie que l'animation n'est pas en cours
                    if(!animationStarted && vehicule == null){
                        vehicule = new Vehicule(1000, "AQUA", Tools.largeurRoute );
                        vehicule.setVehiculeX(X);
                        vehicule.setVehiculeY(Y);
                        conteneurCarte.getChildren().add(vehicule);
                    }
                    else if (!animationStarted) {
                        vehicule.setVehiculeX(X);
                        vehicule.setVehiculeY(Y);
                    }
                    Group paramsVehicule = (Group) ManipScene.getScene().lookup("#paramsVehicule");
                    paramsVehicule.setVisible(true);
                    vehicule.toFront();
                    break;
                case 2:
                    //placement de l'arrivee
                    if(!animationStarted && null == Flag){
                        Flag = new Flag(Tools.largeurRoute);
                        listeEvenement.add(Flag);
                        conteneurCarte.getChildren().add(Flag);
                        Flag.setEvenementX(X);
                        Flag.setEvenementY(Y);
                        System.out.println(Flag.getId());
                    }
                    else if (!animationStarted) {
                        Flag.setEvenementX(X);
                        Flag.setEvenementY(Y);
                        System.out.println(Flag.getId());
                    }
                    Flag.toFront();
                    break;
                case 3:
                    //placement de l'action aller en haut
                    if(!animationStarted && null == direction){
                        up = new Up(Tools.largeurRoute);
                        direction = up;
                        listeEvenement.add(up);
                        conteneurCarte.getChildren().add(up);
                        MainScene.up.setEvenementX(X);
                        MainScene.up.setEvenementY(Y);
                    }
                    else if (!animationStarted){
                        MainScene.up.setEvenementX(X);
                        MainScene.up.setEvenementY(Y);
                    }
                    up.toFront();
                    break;
                case 4:
                    //placement de l'action aller a droite
                    if(!animationStarted && null == direction){
                        right = new Right(Tools.largeurRoute);
                        direction = right;
                        listeEvenement.add(right);
                        conteneurCarte.getChildren().add(right);
                        right.setEvenementX(X);
                        right.setEvenementY(Y);
                    }
                    else if (!animationStarted){
                        right.setEvenementX(X);
                        right.setEvenementY(Y);
                    }
                    right.toFront();
                    break;
                case 5:
                    //placement de l'action aller en bas
                    if(!animationStarted && null == direction){
                        down = new Down(Tools.largeurRoute);
                        direction = down;
                        listeEvenement.add(down);
                        conteneurCarte.getChildren().add(down);
                        down.setEvenementX(X);
                        down.setEvenementY(Y);
                    }
                    else if (!animationStarted){
                        down.setEvenementX(X);
                        down.setEvenementY(Y);
                    }
                    down.toFront();
                    break;
                case 6:
                    //placement de l'action aller a gauche
                    System.out.println();
                    if(!animationStarted && null == direction){
                        Left = new Left(Tools.largeurRoute);
                        direction = Left;
                        listeEvenement.add(Left);
                        conteneurCarte.getChildren().add(Left);
                        Left.setEvenementX(X);
                        Left.setEvenementY(Y);

                    }
                    else if (!animationStarted){
                        Left.setEvenementX(X);
                        Left.setEvenementY(Y);
                    }
                    Left.toFront();
                    break;
            }
        }
    }


}
