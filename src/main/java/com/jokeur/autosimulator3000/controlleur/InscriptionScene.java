package com.jokeur.autosimulator3000.controlleur;
import com.jokeur.autosimulator3000.aide.Tools;
import com.jokeur.autosimulator3000.modele.Utilisateur;
import com.jokeur.autosimulator3000.modele.dao.DAOUtilisateur;

import java.util.ArrayList;
import java.util.Random;
import javafx.scene.image.Image;
import javafx.scene.Node;
import javafx.scene.paint.ImagePattern;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Button;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.scene.text.FontWeight;

public class InscriptionScene {
    /* Fonction qui va renvoyer la scene "Inscription". Positionnement des différents blocs composant la page d'inscription
    ainsi que le contenu "fixe"*/
    public static Group getRoot(Stage stage){
/////Gestion du positionnement des blocs////////////////////////////////////////////////////////////////////////////////
        Group root = new Group();

        // Bloc "A propos"
        double padding = stage.getHeight() * 0.02;
        double largeurGroupePropos = stage.getWidth() * 0.2;
        double hauteurGroupePropos = stage.getHeight() - (padding * 2);
          //Groupe
        Group groupePropos = new Group();
        root.getChildren().add(groupePropos);
          //Rectangle
        Rectangle retanglePropos = new Rectangle(padding, padding, largeurGroupePropos, hauteurGroupePropos);
        retanglePropos.setFill(Color.GRAY);
        groupePropos.getChildren().add(retanglePropos);
        /************************************/
        //Bloc "Inscription"
        double largeurGroupeInscription = stage.getWidth() - largeurGroupePropos - 3*padding;
        double hauteurGroupeInscription = stage.getHeight() - 2*padding;
            //Groupe
        Group groupeInscription = new Group();
        root.getChildren().add(groupeInscription);
        groupeInscription.setTranslateX(largeurGroupePropos + 2*padding);
        groupeInscription.setTranslateY(padding);
            //Rectangle
        Image imgFond = new Image(MainScene.class.getResource(Tools.getProperty("imgFond", "ressources.properties")).toExternalForm());
        Rectangle retangleInscription = new Rectangle(0,0,largeurGroupeInscription, hauteurGroupeInscription);
        groupeInscription.getChildren().add(retangleInscription);
        retangleInscription.setFill(new ImagePattern(imgFond));
        // Bloc "titre"
        Image imgAutoSimulator = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgMiniAutoSimulator", "ressources.properties")).toExternalForm());
        Rectangle retangleTitre = new Rectangle(0,hauteurGroupeInscription*0.02,largeurGroupeInscription, hauteurGroupeInscription*0.35);
        groupeInscription.getChildren().add(retangleTitre);
        retangleTitre.setFill(new ImagePattern(imgAutoSimulator));
            //Bloc "presentation"
        Random rand = new Random();
        int n = rand.nextInt(4);
        ArrayList<Image> listImage = new ArrayList<Image>();
        Rectangle retangleImage = new Rectangle(largeurGroupeInscription*0.4,hauteurGroupeInscription*0.4,largeurGroupeInscription*0.5, hauteurGroupeInscription*0.5);
        Image imgPresentation1 = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgPres1", "ressources.properties")).toExternalForm());
        /************************************/
        /* A CHANGER PAR LA SUITE */
        Image imgPresentation2 = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgPres2", "ressources.properties")).toExternalForm());
        Image imgPresentation3 = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgFondConnexion1", "ressources.properties")).toExternalForm());
        Image imgPresentation4 = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgFondConnexion2", "ressources.properties")).toExternalForm());
        /************************************/
        listImage.add(imgPresentation1);
        listImage.add(imgPresentation2);
        listImage.add(imgPresentation3);
        listImage.add(imgPresentation4);
        groupeInscription.getChildren().add(retangleImage);
        retangleImage.setFill(new ImagePattern(listImage.get(n)));

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////Gestion du contenu des blocs////////////////////////////////////////////////////////////////////////////////////////
        //Bloc "A PROPOS"
            //Titre texte
        Text titreTextePropos = new Text(largeurGroupeInscription*0.1,hauteurGroupeInscription*0.42,"A propos");
        titreTextePropos.setFill(Color.WHITE);
        titreTextePropos.setFont(Font.font("Tahoma", FontWeight.BOLD, 30));
            //Texte dans le cadre
        Text textePropos = new Text(largeurGroupeInscription*0.1,hauteurGroupeInscription*0.46, "Cette application est developpee par:\n-Mickael Larcheveque,\n-Anthony Moniot,\n-Baptiste Leborgne,\n-Guillaume Deman \n\nElle est realisee dans le \ncadre de notre projet JAVA. \n\nVous y simulez la merveilleuse vie d'une voiture dans une ville americaine. \n\nSUPER !");
        textePropos.setFont(Font.font("Tahoma", FontWeight.BOLD, 15));
        textePropos.setFill(Color.WHITE);
        textePropos.setWrappingWidth(largeurGroupePropos*0.9);
        groupeInscription.getChildren().add(titreTextePropos);
        groupeInscription.getChildren().add(textePropos);
        //Bloc "Inscription"
            //Création des groupes
        Group grpFormInscription = new Group();
        Group grpLbFormInscription = new Group();
        groupePropos.getChildren().add(grpFormInscription);
        groupePropos.getChildren().add(grpLbFormInscription);
            //Labels
        Text lbTitreInscription = new Text(largeurGroupePropos*0.17,hauteurGroupePropos*0.07,"Inscrivez-vous");
        lbTitreInscription.setFont(Font.font("Tahoma", FontWeight.BOLD, 23));
        lbTitreInscription.setFill(Color.WHITE);
        Text lbPseudo = new Text(largeurGroupePropos*0.17, hauteurGroupePropos*0.12, "Pseudo:");
        lbPseudo.setFont(Font.font("Tahoma", FontWeight.BOLD, 13));
        lbPseudo.setFill(Color.WHITE);
        Text lbPrenom = new Text(largeurGroupePropos*0.17, hauteurGroupePropos*0.22, "Prenom:");
        lbPrenom.setFont(Font.font("Tahoma", FontWeight.BOLD, 13));
        lbPrenom.setFill(Color.WHITE);
        Text lbNom = new Text(largeurGroupePropos*0.17, hauteurGroupePropos*0.32, "Nom:");
        lbNom.setFont(Font.font("Tahoma", FontWeight.BOLD, 13));
        lbNom.setFill(Color.WHITE);
        Text lbEmail = new Text(largeurGroupePropos*0.17, hauteurGroupePropos*0.42, "Email:");
        lbEmail.setFont(Font.font("Tahoma", FontWeight.BOLD, 13));
        lbEmail.setFill(Color.WHITE);
        Text lbMdp = new Text(largeurGroupePropos*0.17, hauteurGroupePropos*0.52, "Mot de passe:");
        lbMdp.setFont(Font.font("Tahoma", FontWeight.BOLD, 13));
        lbMdp.setFill(Color.WHITE);
        //groupeInscription.getChildren().add(lbTitreInscription);
        groupePropos.getChildren().add(lbTitreInscription);
        grpLbFormInscription.getChildren().add(lbPseudo);
        grpLbFormInscription.getChildren().add(lbPrenom);
        grpLbFormInscription.getChildren().add(lbNom);
        grpLbFormInscription.getChildren().add(lbEmail);
        grpLbFormInscription.getChildren().add(lbMdp);
            //TextBox
        TextField fldPseudo = new TextField();
        grpFormInscription.getChildren().add(fldPseudo);
        fldPseudo.setTranslateX(largeurGroupePropos*0.17);
        fldPseudo.setTranslateY(hauteurGroupePropos*0.13);
        fldPseudo.setMinWidth(largeurGroupePropos*0.75);
        TextField fldPrenom = new TextField();
        grpFormInscription.getChildren().add(fldPrenom);
        fldPrenom.setTranslateX(largeurGroupePropos*0.17);
        fldPrenom.setTranslateY(hauteurGroupePropos*0.23);
        fldPrenom.setMinWidth(largeurGroupePropos*0.75);
        TextField fldNom = new TextField();
        grpFormInscription.getChildren().add(fldNom);
        fldNom.setTranslateX(largeurGroupePropos*0.17);
        fldNom.setTranslateY(hauteurGroupePropos*0.33);
        fldNom.setMinWidth(largeurGroupePropos*0.75);
        TextField fldMail = new TextField();
        grpFormInscription.getChildren().add(fldMail);
        fldMail.setTranslateX(largeurGroupePropos*0.17);
        fldMail.setTranslateY(hauteurGroupePropos*0.43);
        fldMail.setMinWidth(largeurGroupePropos*0.75);
        PasswordField  fldMdp = new PasswordField ();
        grpFormInscription.getChildren().add(fldMdp);
        fldMdp.setTranslateX(largeurGroupePropos*0.17);
        fldMdp.setTranslateY(hauteurGroupePropos*0.53);
        fldMdp.setMinWidth(largeurGroupePropos*0.75);
        //Boutons
        Button btnValider = new Button("Valider");
        grpFormInscription.getChildren().add(btnValider);
        btnValider.setDefaultButton(true);
        btnValider.setMinWidth(largeurGroupePropos*0.3);
        btnValider.setTranslateX(largeurGroupePropos*0.17);
        btnValider.setTranslateY(hauteurGroupePropos*0.61);
        btnValider.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                boolean mdpOk = false;
                boolean pseudoOk = false;
                Utilisateur nouvelUtilisateur = new Utilisateur();

                fldPseudo.setStyle(null);
                fldMdp.setStyle(null);

                try {
                    if (!Tools.isBlank(fldPseudo.getCharacters().toString()) && !DAOUtilisateur.verifPseudo(fldPseudo.getCharacters().toString())){
                        pseudoOk = true;
                    }
                    else {
                        fldPseudo.setStyle("-fx-background-color: red");
                    }
                    if (!Tools.isBlank(fldMdp.getCharacters().toString())){
                        mdpOk = true;
                    }
                    else {
                        fldMdp.setStyle("-fx-background-color: red");
                    }
                }
                catch (Exception exPseudo){
                    System.out.println("Exception: " + exPseudo.getMessage());
                }

                if(pseudoOk && mdpOk){
                    nouvelUtilisateur.setPseudo(fldPseudo.getCharacters().toString());
                    nouvelUtilisateur.setPrenom(fldPrenom.getCharacters().toString());
                    nouvelUtilisateur.setNom(fldNom.getCharacters().toString());
                    nouvelUtilisateur.setEmail(fldMail.getCharacters().toString());
                    nouvelUtilisateur.setMdp(fldMdp.getCharacters().toString());

                    try {
                        DAOUtilisateur.setUtilisateur(nouvelUtilisateur, stage);
                        ManipScene.setConnexionScene(stage);
                    } catch (Exception ex){
                        if("Connexion nulle".equals(ex.getMessage())) {
                            Alert alert = new Alert(Alert.AlertType.ERROR, "Connexion impossible");
                            alert.showAndWait().ifPresent(response -> {
                                try {
                                    if (response == ButtonType.OK) {
                                        ManipScene.setConnexionScene(stage);
                                    }
                                } catch(Exception alertEx) {
                                    System.out.println("Exception: " + alertEx.getMessage());
                                }
                            });
                        }
                        System.out.println("Erreur lors de l'inscription : " + ex.getMessage());
                    }
                }
            }
        });

        Button btnAnnuler = new Button("Annuler");
        grpFormInscription.getChildren().add(btnAnnuler);
        btnAnnuler.setMinWidth(largeurGroupePropos*0.3);
        btnAnnuler.setTranslateX(largeurGroupePropos*0.5);
        btnAnnuler.setTranslateY(hauteurGroupePropos*0.61);
        btnAnnuler.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                try {
                    ManipScene.setConnexionScene(stage);
                }
                catch (Exception ErrAnnul){
                    System.out.println("Exception: " + ErrAnnul.getMessage());
                }
            }
        });

        //Ajout bouton mute
        root.getChildren().add(Musique.addMuteButton());
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        return root;
    }
/////Gestion du positionnement des blocs////
}
