// IMPORT PROJET
package com.jokeur.autosimulator3000.controlleur;
import com.jokeur.autosimulator3000.aide.Tools;
import com.jokeur.autosimulator3000.controlleur.InscriptionScene;
import com.jokeur.autosimulator3000.controlleur.MainScene;
import com.jokeur.autosimulator3000.controlleur.Musique;
import com.jokeur.autosimulator3000.controlleur.ManipScene;
import com.jokeur.autosimulator3000.controlleur.ConnexionScene;
import com.jokeur.autosimulator3000.modele.Utilisateur;
import com.jokeur.autosimulator3000.modele.Vehicule;
import com.jokeur.autosimulator3000.modele.dao.DAOUtilisateur;

// IMPORT JAVA
import java.io.*;
import javafx.application.Application;
import javafx.scene.control.*;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.paint.Paint;
import javafx.scene.input.MouseEvent;
import javafx.scene.image.Image;
import javafx.scene.Node;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.FontWeight;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.util.Duration;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Random;
import java.lang.Object;

public class ConnexionScene {
    // Texte, TextBox & Boutons
    private static Double tailleFont30;
    private static Double tailleFont40;
    private static Double tailleFont16;
    private static Double tailleFont20;
    private static TextField textboxPseudo;
    private static PasswordField textboxPassword;
    private static Button btnConnexion;
    private static Button btnInscription;
    private static Button btnQuitter;
    private static Button btnTriche;

    // Vehicules
    public static Rectangle vehiculeDemo;
    public static int etapeDemo = 1;
    private static double xDemo;
    private static double yDemo;

    // Fond
    public static Rectangle conteneur;
    public static Image imgAutoSimulator = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgAutoSimulator", "ressources.properties")).toExternalForm());
    public static Image imgFond1 = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgFondConnexion1", "ressources.properties")).toExternalForm());
    public static ImagePattern imgPatternFond1 = new ImagePattern(imgFond1);
    public static Image imgFond2 = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgFondConnexion2", "ressources.properties")).toExternalForm());
    public static ImagePattern imgPatternFond2 = new ImagePattern(imgFond2);

    // Fenetre
    public static double largeurConteneur;
    public static double hauteurConteneur;

    public static Group getRoot(Stage stage) throws Exception {

        /***************************************************/
        // MISE EN PLACE DE LA SCENE
        Group root = new Group();

        /***************************************************/
        // IMAGES
        String routeX = null;
        String routeY = null;
        String inter = null;
        String fond = null;
        String voiture = null;
        Image imgRouteX = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgRouteX", "ressources.properties")).toExternalForm());
        Image imgRouteY = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgRouteY", "ressources.properties")).toExternalForm());
        Image imgInter = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgInter", "ressources.properties")).toExternalForm());

        /***************************************************/
        // MENU
        //  Declarations
        double padding = stage.getHeight() * 0.02;
        double largeurMenu = stage.getWidth() * 0.2;
        double hauteurMenu = stage.getHeight() - (padding * 2);
        Rectangle menu = new Rectangle(padding, padding, largeurMenu, hauteurMenu);
        menu.setFill(Color.GRAY);
        //  Position Menu
        Group menuGroup= new Group();
        menuGroup.getChildren().add(menu);
        menuGroup.setTranslateX(largeurMenu + 2*padding);
        menuGroup.setTranslateY(padding);

        /***************************************************/
        // FORMULAIRE
        // Option Texte
        tailleFont30 = stage.getHeight()*0.03;
        tailleFont40 = stage.getHeight()*0.04;
        tailleFont16 = stage.getHeight()*0.016;
        tailleFont20 = stage.getHeight()*0.02;

        // Titre
        Text text = new Text(50, 50, "Connectez-vous");
        //text.setFont(new Font(tailleFont30.intValue()));
        text.setFont(Font.font("Tahoma", FontWeight.BOLD, tailleFont30.intValue()));
        text.setFill(Color.WHITE);

        // TextBox Pseudo
        Text labelPseudo = new Text(50, 80, "Nom d'utilisateur:");
        labelPseudo.setFont(Font.font("Tahoma", FontWeight.BOLD, tailleFont20.intValue()));
        labelPseudo.setFill(Color.WHITE);

        textboxPseudo = new TextField();
        textboxPseudo.setTranslateX(50);
        textboxPseudo.setTranslateY(90);
        textboxPseudo.setMinWidth(largeurMenu * 0.75);

        // TextBox Mot de passe
        Text labelPassword = new Text(50, 150, "Mot de passe:");
        labelPassword.setFont(Font.font("Tahoma", FontWeight.BOLD, tailleFont20.intValue()));
        labelPassword.setFill(Color.WHITE);

        textboxPassword = new PasswordField();
        textboxPassword.setTranslateX(50);
        textboxPassword.setTranslateY(160);
        textboxPassword.setMinWidth(largeurMenu * 0.75);

        // Message erreur
        Text txtErreurLogin = new Text(50, 210, "Utilisateur inconnu");
        txtErreurLogin.setFont(new Font(tailleFont30.intValue()));
        txtErreurLogin.setFill(Color.RED);
        txtErreurLogin.setVisible(false);

        Text txtErreurMdp = new Text(50, 210, "Mauvais mot de passe");
        txtErreurMdp.setFont(new Font(tailleFont30.intValue()));
        txtErreurMdp.setFill(Color.RED);
        txtErreurMdp.setVisible(false);

        // BtnConnexion
        btnConnexion = new Button("Connexion");
        btnConnexion.setTranslateX(50);
        btnConnexion.setTranslateY(230);
        btnConnexion.setDefaultButton(true);
        btnConnexion.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent actionEvent) {
                Utilisateur user = new Utilisateur();
                user.setPseudo(textboxPseudo.getCharacters().toString());
                user.setMdp(textboxPassword.getCharacters().toString());
                txtErreurMdp.setVisible(false);
                txtErreurLogin.setVisible(false);
                try {
                    if(DAOUtilisateur.verifPseudo(user.getPseudo())){
                        if(DAOUtilisateur.verifMdp(user.getMdp())){
                            ManipScene.setActiveUser(DAOUtilisateur.getUtilisateurByPseudo(user.getPseudo()));
                            
                            ManipScene.setMainScene(stage);
                        }
                        else{
                            txtErreurMdp.setVisible(true);
                        }
                    }
                    else{
                        txtErreurLogin.setVisible(true);
                    }
                } catch (Exception ex){
                    if("Connexion nulle".equals(ex.getMessage())) {
                        Alert alert = new Alert(Alert.AlertType.ERROR, "Connexion impossible");
                        alert.showAndWait().ifPresent(response -> {
                            try {
                                if (response == ButtonType.OK) {
                                    ManipScene.setConnexionScene(stage);
                                }
                            } catch(Exception alertEx) {
                                System.out.println("Exception: " + alertEx.getMessage());
                            }
                        });
                    }
                    System.out.println("Erreur lors de la connexion : " + ex.getMessage());
                }
            }
        });

        // BtnInscription
        btnInscription = new Button("Inscription");
        btnInscription.setTranslateX(150);
        btnInscription.setTranslateY(230);
        btnInscription.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    ManipScene.setInscriptionScene(stage);
                } catch (Exception e){
                    System.out.println("Erreur lors de l'appel a la page inscription : " + e.getMessage());
                }
            }
        });

        // BtnQuitter
        btnQuitter = new Button("Quitter");
        btnQuitter.setTranslateX(largeurMenu*0.45);
        btnQuitter.setTranslateY(hauteurMenu*0.95);
        btnQuitter.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    stage.hide();
                } catch (Exception e){
                    System.out.println("Erreur lors de la deconnexion : " + e.getMessage());
                }
            }
        });

        btnTriche = new Button("Tricher");
        btnTriche.setTranslateX(largeurMenu*0.45);
        btnTriche.setTranslateY(hauteurMenu*0.75);
        btnTriche.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent actionEvent) {
                Utilisateur user = new Utilisateur();
                user.setPseudo((Tools.getProperty("userTriche", "config.properties")).toString());
                user.setMdp((Tools.getProperty("passwordTriche", "config.properties")).toString());
                txtErreurMdp.setVisible(false);
                txtErreurLogin.setVisible(false);
                try {
                    if(DAOUtilisateur.verifPseudo(user.getPseudo())){
                        if(DAOUtilisateur.verifMdp(user.getMdp())){
                            ManipScene.setActiveUser(DAOUtilisateur.getUtilisateurByPseudo(user.getPseudo()));
                            ManipScene.setMainScene(stage);
                        }
                        else{
                            txtErreurMdp.setVisible(true);
                        }
                    }
                    else{
                        txtErreurLogin.setVisible(true);
                    }
                } catch (Exception ex){
                    if("Connexion nulle".equals(ex.getMessage())) {
                        Alert alert = new Alert(Alert.AlertType.ERROR, "Connexion impossible");
                        alert.showAndWait().ifPresent(response -> {
                            try {
                                if (response == ButtonType.OK) {
                                    ManipScene.setConnexionScene(stage);
                                }
                            } catch(Exception alertEx) {
                                System.out.println("Exception: " + alertEx.getMessage());
                            }
                        });
                    }
                    System.out.println("Erreur lors de la connexion : " + ex.getMessage());
                }
            }
        });

        /***************************************************/
        // CONTENEUR
        //  Declarations
        largeurConteneur = stage.getWidth() - largeurMenu - 3*padding;
        hauteurConteneur = stage.getHeight() - 2*padding;
        conteneur = new Rectangle(0, 0, largeurConteneur, hauteurConteneur);

        //  Position Conteneur
        Group conteneurGroup= new Group();
        conteneurGroup.getChildren().add(conteneur);
        conteneurGroup.setTranslateX(largeurMenu + 2*padding);
        conteneurGroup.setTranslateY(padding);

        //  Position image AutoSimulator + declaration
        Rectangle titre = new Rectangle(0, 0, largeurConteneur, hauteurConteneur);
        titre.setFill(new ImagePattern(imgAutoSimulator));
        titre.setId("titre");
        Group conteneurImage= new Group();
        conteneurImage.getChildren().add(titre);
        conteneurImage.setTranslateX(largeurMenu + 2*padding);
        conteneurImage.setTranslateY(padding);

        /***************************************************/
        // DECLARATION
        List<Double[]> listRoutesVert = new ArrayList();
        List<Double[]> listRoutesHor = new ArrayList();
        int axeVert = 0;
        int axeHor = 0;
        int emplacementVoiture = 0;
        double emplacementVoitureX = 0;
        double emplacementVoitureY = 0;
        double largeurRoute = hauteurConteneur*0.08;

        /***************************************************/
        // ROUTES CREATION
        conteneurGroup.getChildren().add(Tools.genererCarteDefaut(largeurConteneur, hauteurConteneur, false));

        /***************************************************/
        // AJOUT DU VEHICULE
        vehiculeDemo = Tools.makeVehicule(largeurConteneur*0.15, hauteurConteneur*0.160, largeurRoute, Vehicule.imgCarRight);
        vehiculeDemo.setId("vehiculeDemo");
        conteneurGroup.getChildren().add(vehiculeDemo);

        /***************************************************/
        // AJOUT DES ELEMENTS ET LANCEMENT
        root.getChildren().add(menu);
        root.getChildren().add(conteneurGroup);
        root.getChildren().add(conteneurImage);
        root.getChildren().add(text);
        root.getChildren().add(labelPseudo);
        root.getChildren().add(labelPassword);
        root.getChildren().add(textboxPseudo);
        root.getChildren().add(textboxPassword);
        root.getChildren().add(txtErreurLogin);
        root.getChildren().add(txtErreurMdp);
        root.getChildren().add(btnConnexion);
        root.getChildren().add(btnInscription);
        root.getChildren().add(btnQuitter);
        root.getChildren().add(btnTriche);
        root.getChildren().add(Musique.addMuteButton());
        root.getChildren().add(Musique.addPoneyButton());

        return root;
    }

    public static void parcourDemo(){
        xDemo = vehiculeDemo.getBoundsInLocal().getMinX();
        yDemo = vehiculeDemo.getBoundsInLocal().getMinY();

        switch(etapeDemo) {
            case 1:
                vehiculeDemo.setX(xDemo + 3);
                vehiculeDemo.setFill(Vehicule.imgPatternCarRight);
                conteneur.setFill(imgPatternFond1);
                if(xDemo > largeurConteneur*0.312) { //0.302
                    etapeDemo = 2;
                }
                break;
            case 2:
                vehiculeDemo.setY(yDemo + 3);
                vehiculeDemo.setFill(Vehicule.imgPatternCarDown);
                conteneur.setFill(imgPatternFond1);
                if(yDemo > hauteurConteneur*0.600) { //0.592
                    etapeDemo = 3;
                }
                break;
            case 3:
                vehiculeDemo.setX(xDemo + 3);
                vehiculeDemo.setFill(Vehicule.imgPatternCarRight);
                conteneur.setFill(imgPatternFond1);
                if(xDemo > largeurConteneur*0.785) {
                    etapeDemo = 4;
                }
                break;
            case 4:
                vehiculeDemo.setY(yDemo - 3);
                vehiculeDemo.setFill(Vehicule.imgPatternCarUp);
                conteneur.setFill(imgPatternFond2);
                if(yDemo < hauteurConteneur*0.300) {
                    etapeDemo = 5;
                }
                break;
            case 5:
                vehiculeDemo.setX(xDemo - 3);
                vehiculeDemo.setFill(Vehicule.imgPatternCarLeft);
                if(xDemo < largeurConteneur*0.160) {
                    etapeDemo = 6;
                }
                break;
            case 6:
                vehiculeDemo.setY(yDemo - 3);
                vehiculeDemo.setFill(Vehicule.imgPatternCarUp);
                if(yDemo < hauteurConteneur*0.15) {
                    etapeDemo = 7;
                }
                break;
            default:
                vehiculeDemo.setX(largeurConteneur*0.16);
                vehiculeDemo.setY(hauteurConteneur*0.15);
                etapeDemo = 1;
        }
    }
}
