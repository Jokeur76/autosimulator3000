package com.jokeur.autosimulator3000.controlleur;

import java.io.*;
import com.jokeur.autosimulator3000.aide.Tools;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.scene.control.*;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Rectangle;
import com.jokeur.autosimulator3000.modele.Vehicule;
import com.jokeur.autosimulator3000.controlleur.ConnexionScene;
import com.jokeur.autosimulator3000.controlleur.ManipScene;
import java.io.File;
import java.net.URL;
import java.util.Random;

public class Bruitage {
    //private static MediaPlayer bruitage = null;
    private static MediaPlayer mpDemarre = null;
    private static MediaPlayer mpWin = null;
    private static MediaPlayer mpFrein = null;
    private static MediaPlayer mpKlaxon = null;
    private static MediaPlayer mpAccident = null;
    private static MediaPlayer mpRoule = null;
    private static String played = null;

    public static void playDemarre(String toPlay) {
        if(null == played || !toPlay.equals(played)) {
            startDemarre(toPlay);
        }
        mpDemarre.play();
    }

    public static void playWin(String toPlay) {
        if(null == played || !toPlay.equals(played)) {
            startWin(toPlay);
        }
        mpWin.play();
    }

    public static void playFrein(String toPlay) {
        if(null == played || !toPlay.equals(played)) {
            startFrein(toPlay);
        }
        mpFrein.play();
    }

    public static void playKlaxon(String toPlay) {
        if(null == played || !toPlay.equals(played)) {
            startKlaxon(toPlay);
        }
        mpKlaxon.play();
    }

    public static void playAccident(String toPlay) {
        if(null == played || !toPlay.equals(played)) {
            startAccident(toPlay);
        }
        mpAccident.play();
    }

    public static void playRoule(String toPlay) {
        if(null == played || !toPlay.equals(played)) {
            startRoule(toPlay);
        }
        mpRoule.setVolume(0.5);
        mpRoule.play();
    }

    /** Declaration des son */
    public static void sonDemarre() {
        playDemarre("son/bruitage/demarre.mp3");
    }

    public static void sonWin() {
        playWin("son/bruitage/end.mp3");
    }

    public static void sonFrein() {
        playFrein("son/bruitage/frein.mp3");
    }

    public static void sonKlaxon() {
        playKlaxon("son/bruitage/klaxon.mp3");
    }

    public static void sonAccident() {
        playAccident("son/bruitage/accident.mp3");
    }

    public static void sonRoule() {
        playRoule("son/bruitage/roule.mp3");
    }

    public static void sonRouleStop() {
        mpRoule.stop();
        sonFrein();
        //mpDemarre.stop();
    }

    /**
     * Met la musique en pause.
     */
    public static void stop() {
        mpDemarre.stop();
        mpWin.stop();
        mpFrein.stop();
        mpKlaxon.stop();
    }

    /**
     * PRIVATE FUNCTIONS
    */
    private static void startDemarre(String toPlay) {
        if(null != mpDemarre) {
            mpDemarre = null;
        }
        played = toPlay;
        URL fileToPlay = Musique.class.getResource("/" + played);
        Media sound = new Media(fileToPlay.toString());
        mpDemarre = new MediaPlayer(sound);
     }

     private static void startWin(String toPlay) {
         if(null != mpWin) {
             mpWin = null;
         }
         played = toPlay;
         URL fileToPlay = Musique.class.getResource("/" + played);
         Media sound = new Media(fileToPlay.toString());
         mpWin = new MediaPlayer(sound);
    }

    private static void startFrein(String toPlay) {
        if(null != mpFrein) {
            mpFrein = null;
        }
        played = toPlay;
        URL fileToPlay = Musique.class.getResource("/" + played);
        Media sound = new Media(fileToPlay.toString());
        mpFrein = new MediaPlayer(sound);
     }

     private static void startKlaxon(String toPlay) {
         if(null != mpKlaxon) {
             mpKlaxon = null;
         }
         played = toPlay;
         URL fileToPlay = Musique.class.getResource("/" + played);
         Media sound = new Media(fileToPlay.toString());
         mpKlaxon = new MediaPlayer(sound);
    }

    private static void startAccident(String toPlay) {
        if(null != mpAccident) {
            mpAccident = null;
        }
        played = toPlay;
        URL fileToPlay = Musique.class.getResource("/" + played);
        Media sound = new Media(fileToPlay.toString());
        mpAccident = new MediaPlayer(sound);
   }

   private static void startRoule(String toPlay) {
       if(null != mpRoule) {
           mpRoule = null;
       }
       played = toPlay;
       URL fileToPlay = Musique.class.getResource("/" + played);
       Media sound = new Media(fileToPlay.toString());
       mpRoule = new MediaPlayer(sound);
  }

}
