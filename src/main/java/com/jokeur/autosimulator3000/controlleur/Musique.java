package com.jokeur.autosimulator3000.controlleur;

import java.io.*;
import com.jokeur.autosimulator3000.aide.Tools;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.scene.control.*;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import com.jokeur.autosimulator3000.modele.Vehicule;
import com.jokeur.autosimulator3000.controlleur.ConnexionScene;
import com.jokeur.autosimulator3000.controlleur.ManipScene;
import java.io.File;
import java.net.URL;
import java.util.Random;

public class Musique {
    private static Image imgAutoSimulator = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgAutoSimulator", "ressources.properties")).toExternalForm());
    private static Image imgPoney = new Image(ConnexionScene.class.getResource(Tools.getProperty("imgPoney", "ressources.properties")).toExternalForm());

    private static MediaPlayer musiqueFond = null;
    private static String played = null;
    private static Rectangle muteButton;
    private static ImagePattern soundOn = new ImagePattern(new Image(ConnexionScene.class.getResource(Tools.getProperty("soundOn", "ressources.properties")).toExternalForm()));
    private static ImagePattern soundOff = new ImagePattern(new Image(ConnexionScene.class.getResource(Tools.getProperty("soundOff", "ressources.properties")).toExternalForm()));
    private static Rectangle poney = new Rectangle();
    /**
     * Lance la musique demandé en paramètre paramètre.
     * Si l'objet n'existe pas il sera créé
     * @param toPlay
     */
    public static void play(String toPlay) {
        if(null == played || !toPlay.equals(played)) {
            start(toPlay);
        }
        musiqueFond.play();
        musiqueFond.setVolume(0.4);
        musiqueFond.setOnEndOfMedia(new Runnable()
        {
            public void run()
            {
                playRandom();
            }
        });
    }

    public static void playRandom() {
        Random rand = new Random();
        int n = rand.nextInt(7) + 1;
        play("son/" + n +".mp3");
        if(n == 7){
          poney.setVisible(true);
        }
        else{
          poney.setVisible(false);
        }
    }

    /**
     * Met la musique en pause.
     */
    public static void stop() {
        musiqueFond.stop();
    }

    /**
     * Créer un bouton pour mute/unmute la musique.
     * Retourne un objet rectangle préparamétré.
     * @return Rectangle
     */
    public static Rectangle addPoneyButton() {
        //muteButton = new Rectangle();
        poney.setHeight(ManipScene.getScreenHeight() * 0.2);
        poney.setWidth(ManipScene.getScreenHeight() * 0.2);
        poney.setX(ManipScene.getScreenHeight() * 0.1);
        poney.setY(ManipScene.getScreenHeight() - poney.getHeight() - ManipScene.getScreenHeight() * 0.4);
        poney.setFill(new ImagePattern(imgPoney));
        poney.setVisible(false);
        return poney;
    }

    public static Rectangle addMuteButton() {
        if(null == muteButton) {
            muteButton = new Rectangle();
            muteButton.setFill(soundOff);
        }
        muteButton.setHeight(ManipScene.getScreenHeight() * 0.05);
        muteButton.setWidth(ManipScene.getScreenHeight() * 0.05);
        muteButton.setX(ManipScene.getScreenHeight() * 0.04);
        muteButton.setY(ManipScene.getScreenHeight() - muteButton.getHeight() - ManipScene.getScreenHeight() * 0.04);

        muteButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if("PLAYING".equals(musiqueFond.getStatus().name())) {
                    muteButton.setFill(soundOn);
                    Musique.stop();
                } else {
                    muteButton.setFill(soundOff);
                    Musique.playRandom();
                }
            }
        });

        return muteButton;
    }

    /**
     * PRIVATE FUNCTIONS
    */
     private static void start(String toPlay) {
         if(null != musiqueFond) {
             musiqueFond.stop();
             musiqueFond = null;
         }
         played = toPlay;
         URL fileToPlay = Musique.class.getResource("/" + played);
         Media sound = new Media(fileToPlay.toString());
         musiqueFond = new MediaPlayer(sound);
    }

}
