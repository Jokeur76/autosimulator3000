package com.jokeur.autosimulator3000.controlleur;

import com.jokeur.autosimulator3000.aide.Tools;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.stage.Screen;
import javafx.stage.Stage;
import com.jokeur.autosimulator3000.modele.Utilisateur;
import com.jokeur.autosimulator3000.modele.dao.DAOUtilisateur;

import javax.rmi.CORBA.Util;

public class ManipScene {
    private static int sceneActuelle;
    private static double screenHeight = Screen.getPrimary().getVisualBounds().getHeight();
    private static double screenWidth = Screen.getPrimary().getVisualBounds().getWidth();
    private static Utilisateur activeUser = new Utilisateur();
    private static Stage stageActuel;

    public static Utilisateur getActiveUser() {
        return activeUser;
    }
    public static void setActiveUser(Utilisateur user) {
        activeUser = user;
    }

    public static void nopActiveUser(Utilisateur user) {
        activeUser = null;
    }

    public static int getSceneActuelle() {
        return sceneActuelle;
    }

    public static double getScreenHeight() {
        return screenHeight;
    }

    public static double getScreenWidth() {
        return screenWidth;
    }

    public static Stage getStage(){
        return stageActuel;
    }

    public static Scene getScene(){
        return stageActuel.getScene();
    }

    public static void setConnexionScene(Stage stage) throws Exception {

        stageActuel = stage;

        sceneActuelle = 0;
        stage.setScene(new Scene(new Region()));
        stage.getScene().setFill(Color.WHITE);
        stage.getScene().setRoot(ConnexionScene.getRoot(stage));
    }

    public static void setMainScene(Stage stage) throws Exception {
        sceneActuelle = 1;
        stage.setScene(new Scene(new Region()));
        Image imgFond = new Image(MainScene.class.getResource(Tools.getProperty("imgFond", "ressources.properties")).toExternalForm());
        stage.getScene().setFill(new ImagePattern(imgFond));
        stage.getScene().setRoot(MainScene.getRoot(stage));
    }

    public static void setInscriptionScene(Stage stage) throws Exception {
        sceneActuelle = 2;
        stage.setScene(new Scene(new Region()));
        stage.getScene().setFill(Color.WHITE);
        stage.getScene().setRoot(InscriptionScene.getRoot(stage));
    }

    /**
     * PRIVATES FUNCTION
     */
}
